#include "StdAfx.h"
#include "DUITransitionTreeCtrl.h"

namespace ED
{
	// 	BEGIN_MSG_MAP(DUITransitionTreeCtrl)
	// 		MSG_WM_COMMAND(OnCommand)
	// 	END_MSG_MAP()

	BEGIN_EVENT_MAP(DUITransitionTreeCtrl)
		EVENT_NAME_COMMAND(L"Transitionaddbutton", OnTransitionMenuExpand)
		END_EVENT_INBASE()// 基类

		DUITransitionTreeCtrl*  DUITransitionTreeCtrl::ms_pthis = NULL;
		DUITransitionTreeCtrl::DUITransitionTreeCtrl() :m_hOldSelectItem(NULL), m_bMsgHandled(FALSE), m_hMenuHoverItem(NULL), m_pCurShowJsonParser(NULL),
			m_iDelayFrame(0), m_iFadeFrame(0), m_iLastingFrame(0), m_iPlayLoop(0)
		{
			ms_pthis = this;
			m_EventMgr.SubscribeEvent(DM::DMEventTCSelChangedArgs::EventID, Subscriber(&DUITransitionTreeCtrl::OnTreeItemSelectChanged, this));
		}

		DUITransitionTreeCtrl::~DUITransitionTreeCtrl()
		{
			m_pCurShowJsonParser = NULL;
		}

		BOOL DUITransitionTreeCtrl::s_ProcessWindowMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT& lResult, DWORD dwMsgMapID /*= 0*/)
		{
			if (ms_pthis)
			{
				//ms_pthis->ProcessWindowMessage(hWnd, uMsg, wParam, lParam, lResult, dwMsgMapID);
			}
			return FALSE;
		}

		DMCode DUITransitionTreeCtrl::s_DMHandleEvent(DM::DMEventArgs *pEvt)
		{
			if (ms_pthis)
			{
				if (DMEventOfPanelArgs::EventID == pEvt->GetEventID())
				{
					DMEventOfPanelArgs* pEvent = (DMEventOfPanelArgs*)pEvt;
					return ms_pthis->DMHandleEvent(pEvent->m_pOrgEvt);
				}
				else
				{
					return ms_pthis->DMHandleEvent(pEvt);// 转发
				}
			}

			return DM_ECODE_FAIL;
		}

		void DUITransitionTreeCtrl::OnCommand(UINT uNotifyCode, int nID, HWND wndCtl, EDJSonParserPtr pJsonParser, bool bForceRefresh)
		{
			if (nID < TransitionMenu_BASE || nID >= TransitionMenu_MAX)
			{
				return;
			}
			if (TransitionMenu_AddTransition == nID) //添加transition
			{
				EDJSonParserPtr pCdtParser = pJsonParser->CreateChildParser(EDConditionsParser::GetClassNameW());
				pCdtParser->CreateChildParser(EDConditionsNodeParser::GetClassNameW());
				EDJSonParserPtr pTargetParser = pJsonParser->CreateChildParser(EDTargetsParser::GetClassNameW());
				pTargetParser->CreateChildParser(EDTargetsNodeParser::GetClassNameW());
				return;
			}
			else if (TransitionMenu_ShowTransition == nID) //显示transition
			{
				if (!pJsonParser || pJsonParser == m_pCurShowJsonParser && !bForceRefresh)
					return;

				RemoveAllItems();
				DMXmlDocument Doc;
				DMXmlNode XmlItem = Doc.Base();

				EDJSonParser* pConditionsParser = pJsonParser->FindChildParserByClassName(EDConditionsParser::GetClassNameW());
				if (pConditionsParser)
				{
					CStringW strConditionName = L"Condition"; int iIndex = 0;
					EDConditionsNodeParser* pCdtNodeParser = dynamic_cast<EDConditionsNodeParser*>(pConditionsParser->GetParser(GPS_FIRSTCHILD));
					while (pCdtNodeParser)
					{
						if (iIndex++ != 0)
							strConditionName.Format(L"Condition%d", iIndex);
						InitConditionMajorXml(XmlItem, pCdtNodeParser, strConditionName);
						pCdtNodeParser = dynamic_cast<EDConditionsNodeParser*>(pCdtNodeParser->GetParser(GPS_NEXTSIBLING));
					}
				}
				
				EDJSonParser* pTargetsParser = pJsonParser->FindChildParserByClassName(EDTargetsParser::GetClassNameW());
				if (pTargetsParser)
				{
					CStringW strTargetName = L"Target"; int iIndex = 0;
					EDTargetsNodeParser* pTargetsNodeParser = dynamic_cast<EDTargetsNodeParser*>(pTargetsParser->GetParser(GPS_FIRSTCHILD));
					while (pTargetsNodeParser)
					{
						if (iIndex++ != 0)
							strTargetName.Format(L"Target%d", iIndex);
						InitTargetMajorXml(XmlItem, pTargetsNodeParser, strTargetName);
						pTargetsNodeParser = dynamic_cast<EDTargetsNodeParser*>(pTargetsNodeParser->GetParser(GPS_NEXTSIBLING));
					}
				}

				InitRandomShowNodeXml(XmlItem, dynamic_cast<EDTransitionsNodeParser*>(pJsonParser));
				LoadBranch(NULL, XmlItem.FirstChild(DMAttr::DUITreeCtrlExAttr::NODE_treeitem), DMTVI_LAST);
				m_pCurShowJsonParser = pJsonParser;
				return;
			}
			else if (TransitionMenu_DeleTransition == nID)
			{
				g_pMainWnd->HandleEffectTreeMenu(EFFECTTREEMENU_DELEEFFECT, NULL, NULL);
			}
			else if (TransitionMenu_AddCondition == nID)
			{
				EDJSonParser *pConditionPs = m_pCurShowJsonParser->FindChildParserByClassName(EDConditionsParser::GetClassNameW());
				if (!pConditionPs)
				{
					pConditionPs = m_pCurShowJsonParser->CreateChildParser(EDConditionsParser::GetClassNameW());
				}
				pConditionPs->CreateChildParser(EDConditionsNodeParser::GetClassNameW());
				OnCommand(0, TransitionMenu_ShowTransition, 0, m_pCurShowJsonParser, true);
			}
			else if (TransitionMenu_AddTarget == nID)
			{
				EDJSonParser *pTargetPs = m_pCurShowJsonParser->FindChildParserByClassName(EDTargetsParser::GetClassNameW());
				if (!pTargetPs)
				{
					pTargetPs = m_pCurShowJsonParser->CreateChildParser(EDTargetsParser::GetClassNameW());
				}
				EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pTargetPs->CreateChildParser(EDTargetsNodeParser::GetClassNameW()));
				OnCommand(0, TransitionMenu_ShowTransition, 0, m_pCurShowJsonParser, true);
			}
			else if (TransitionMenu_DeleCondition == nID)
			{
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(m_hMenuHoverItem);
				pData->m_pJSonParser->DestoryParser();
				pData->m_pJSonParser = NULL;

				OnCommand(0, TransitionMenu_ShowTransition, 0, m_pCurShowJsonParser, true);
			}
			else if (TransitionMenu_AddEvent == nID)
			{
				int iIndex = 1;
				HDMTREEITEM hInsertAfter = DMTVI_LAST;
				HDMTREEITEM hChildItem = GetChildItem(m_hMenuHoverItem);
				while (hChildItem)
				{
					TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hChildItem);
					if (pData && pData->m_treeTag == TagConditionEventNode)//Event节点
					{
						hInsertAfter = hChildItem;
						iIndex++;
					}
					hChildItem = GetNextSiblingItem(hChildItem);
				}
				CStringW strEventName = L"Event";
				if (iIndex > 1)
				{
					strEventName.Format(L"Event%d", iIndex);
				}

				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(m_hMenuHoverItem);
				EDConditionsNodeParser* pCdtNodeParser = dynamic_cast<EDConditionsNodeParser*>(pData->m_pJSonParser); DMASSERT(pCdtNodeParser);
				if (pCdtNodeParser)
				{
					if (!pCdtNodeParser->m_strTriggers.IsEmpty())
					{
						pCdtNodeParser->m_strTriggers += L"&";
					}
					pCdtNodeParser->m_strTriggers += L"H.face.appear";
				}
				OnCommand(0, TransitionMenu_ShowTransition, 0, m_pCurShowJsonParser, true);
			}
			else if (TransitionMenu_DeleEvent == nID)
			{
				int iMeIndex = 0; //删除的event的index
				HDMTREEITEM hPrevItem = GetPrevSiblingItem(m_hMenuHoverItem);
				while (hPrevItem)
				{
					iMeIndex++;
					hPrevItem = GetPrevSiblingItem(hPrevItem);
				}
				int iEventIndex = (iMeIndex - 2) / 3;
				HDMTREEITEM hParentItem = GetParentItem(m_hMenuHoverItem);
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
				EDConditionsNodeParser* pCditNodePs = dynamic_cast<EDConditionsNodeParser*>(pData->m_pJSonParser); DMASSERT(pCditNodePs);
				if (pCditNodePs)
				{
					CArray<CStringW> ArrayTriggers;
					SplitStringT(pCditNodePs->m_strTriggers, L'&', ArrayTriggers);
					pCditNodePs->m_strTriggers.Empty();
					for (size_t i = 0; i < ArrayTriggers.GetCount(); i++)
					{
						if (iEventIndex == i)
						{
							continue;
						}
						if (!pCditNodePs->m_strTriggers.IsEmpty())
						{
							pCditNodePs->m_strTriggers += L"&";
						}
						pCditNodePs->m_strTriggers += ArrayTriggers[i];
					}
				}
				OnCommand(0, TransitionMenu_ShowTransition, 0, m_pCurShowJsonParser, true);

			}
			else if (TransitionMenu_DeleTarget == nID)
			{

				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(m_hMenuHoverItem);
				pData->m_pJSonParser->DestoryParser();
				pData->m_pJSonParser = NULL;

				OnCommand(0, TransitionMenu_ShowTransition, 0, m_pCurShowJsonParser, true);
			}
			else
				DMASSERT(FALSE);
		}

		DMCode DUITransitionTreeCtrl::OnTransitionMenuExpand()
		{
			m_hMenuHoverItem = NULL;
			PopTransitionTreeMenu(HDMTREEITEM(-100));
			return DM_ECODE_OK;
		}

		void DUITransitionTreeCtrl::OnRButtonDown(UINT nFlags, CPoint pt)
		{
			do
			{
				if (!m_bRightClickSel)
				{
					break;
				}
				m_hHoverItem = HitTest(pt);
				if (m_hHoverItem != m_hSelItem
					&& m_hHoverItem)
				{
					SelectItem(m_hHoverItem, false);
				}

				// 是否弹出菜单
				if (NULL == m_hHoverItem || DMTVI_ROOT == m_hHoverItem)
				{
					break;
				}

				DM::LPTVITEMEX pData = GetItem(m_hHoverItem);
				if (!pData)
					break;

				DUIRichEdit* pItemEdit = pData->pPanel->FindChildByNameT<DUIRichEdit>(L"treeitemedit", true);
				if (pItemEdit)
				{
					pItemEdit->DM_OnKillFocus();  //右键弹出的时候   取消重命名状态
				}

				m_hMenuHoverItem = m_hHoverItem;
				PopTransitionTreeMenu(m_hHoverItem);
			} while (false);
		}

		DMCode DUITransitionTreeCtrl::PopTransitionTreeMenu(HDMTREEITEM hItem)
		{
			DMASSERT(hItem);
			if (NULL == hItem || DMTVI_ROOT == hItem)
			{
				return DM_ECODE_FAIL;
			}

			TransitionTreeDataPtr pData = NULL;
			DMBufT<TransitionTreeData> pAutoData;
			if (HDMTREEITEM(-100) == hItem)
			{
				pAutoData.Allocate(1);
				pData = pAutoData.get();
				pData->m_treeTag = TagTransitionNode;
			}
			else
			{
				pData = (TransitionTreeDataPtr)GetItemData(hItem);
			}
			if (pData == NULL)
				return DM_ECODE_NOTIMPL;

			DMXmlDocument Doc;
			g_pDMApp->InitDMXmlDocument(Doc, XML_LAYOUT, L"ds_menu_respanel");
			DMXmlNode XmlNode = Doc.Root();
			XmlNode.SetAttribute(L"MaxWidth", L"120");

			if (TagTransitionNode == pData->m_treeTag)//transition1 transition2
			{
				DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
				XmlItem.SetAttribute(XML_ID, IntToString(g_TransitionMenuItem[TransitionMenu_AddCondition - TransitionMenu_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_TransitionMenuItem[TransitionMenu_AddCondition - TransitionMenu_BASE].text);
				XmlItem = XmlNode.InsertChildNode(XML_ITEM);
				XmlItem.SetAttribute(XML_ID, IntToString(g_TransitionMenuItem[TransitionMenu_AddTarget - TransitionMenu_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_TransitionMenuItem[TransitionMenu_AddTarget - TransitionMenu_BASE].text);
			}
			else if (TagConditionNode == pData->m_treeTag) //condition1 condition2
			{
				DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
				XmlItem.SetAttribute(XML_ID, IntToString(g_TransitionMenuItem[TransitionMenu_DeleCondition - TransitionMenu_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_TransitionMenuItem[TransitionMenu_DeleCondition - TransitionMenu_BASE].text);
				XmlItem = XmlNode.InsertChildNode(XML_ITEM);
				XmlItem.SetAttribute(XML_ID, IntToString(g_TransitionMenuItem[TransitionMenu_AddEvent - TransitionMenu_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_TransitionMenuItem[TransitionMenu_AddEvent - TransitionMenu_BASE].text);
			}
			else if (TagConditionEventNode == pData->m_treeTag)//condition 下面的event
			{
				DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
				XmlItem.SetAttribute(XML_ID, IntToString(g_TransitionMenuItem[TransitionMenu_DeleEvent - TransitionMenu_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_TransitionMenuItem[TransitionMenu_DeleEvent - TransitionMenu_BASE].text);
			}
			else if (TagTargetNode == pData->m_treeTag)//transition 下面的target
			{
				DMXmlNode XmlItem = XmlNode.InsertChildNode(XML_ITEM);
				XmlItem.SetAttribute(XML_ID, IntToString(g_TransitionMenuItem[TransitionMenu_DeleTarget - TransitionMenu_BASE].id)); XmlItem.SetAttribute(XML_TEXT, g_TransitionMenuItem[TransitionMenu_DeleTarget - TransitionMenu_BASE].text);
			}

			DUIMenu Menu;
			Menu.LoadMenu(XmlNode);
			POINT pt;
			GetCursorPos(&pt);
			Menu.TrackPopupMenu(0, pt.x, pt.y, g_pMainWnd->m_hWnd);
			return DM_ECODE_OK;
		}

		DMCode DUITransitionTreeCtrl::OnTreeAddBtnClicked(DMEventArgs *pEvt)
		{
			do
			{
				if (!pEvt)
					break;
				DUIButton* pTreeAddBtn = dynamic_cast<DUIButton*>(pEvt->m_pSender);
				if (!pTreeAddBtn)
					break;

				DUIWindow* pParent = pTreeAddBtn->DM_GetWindow(GDW_PARENT);
				DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
				if (!pPanel)
					break;

				HDMTREEITEM hItem = (HDMTREEITEM)pPanel->GetItemId();
				if (!hItem || NULL == g_pMainWnd)
					break;

				m_hMenuHoverItem = hItem;
				PopTransitionTreeMenu(hItem);
			} while (false);

			return DM_ECODE_OK;
		}

		HDMTREEITEM DUITransitionTreeCtrl::ComboSeleChgTreeItem(DMEventArgs* pEvent)
		{
			do
			{
				if (!pEvent)
					break;

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;

				DUIWindow* pParent = pListBox->DM_GetWindow(GDW_OWNER);//combobox
				if (!pParent)
					break;

				pParent = pParent->DM_GetWindow(GDW_PARENT);
				DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
				if (!pPanel)
					break;

				HDMTREEITEM hItem = (HDMTREEITEM)pPanel->GetItemId();
				return hItem;
			} while (false);
			return (HDMTREEITEM)NULL;
		}

		DM::HDMTREEITEM DUITransitionTreeCtrl::EditSelChgTreeItem(DMEventArgs* pEvent)
		{
			HDMTREEITEM hRetItem = NULL;
			do
			{
				if (!pEvent)
					break;

				DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
				if (!pEdit)
					break;

				DUIComboBox* pComboBox = dynamic_cast<DUIComboBox*>(pEdit->DM_GetWindow(GDW_OWNER));
				if (!pComboBox)
					break;

				DUIWindow* pParent = pComboBox->DM_GetWindow(GDW_PARENT);
				DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
				if (!pPanel)
					break;

				hRetItem = (HDMTREEITEM)pPanel->GetItemId();
			} while (false);
			return hRetItem;
		}

		DMCode DUITransitionTreeCtrl::EnableComboItem(HDMTREEITEM hItem, BOOL bEnable, BOOL bResetSelect)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!hItem)
					break;

				DM::LPTVITEMEX pData = GetItem(hItem);
				if (!pData)
					break;

				DUIComboBox* pCombobox = pData->pPanel->FindChildByNameT<DUIComboBox>(COMBOBOXNAME, true); DMASSERT(pCombobox);
				if (!pCombobox)
					break;

				pCombobox->DM_EnableWindow(bEnable);
				if (pCombobox->m_pEdit && pCombobox->m_pEdit->DM_IsVisible()) //edit 不能设置clrtextdisable属性  所以用这种方式间接设置
				{
					pCombobox->m_pEdit->SetAttribute(L"clrtext", bEnable ? L"pbgra(ff, ff, ff, d0)" : L"pbgra(30,30,30,d0)");
				}
				if (bResetSelect){
					DUIWndAutoMuteGuard guard(pCombobox->GetListBox());
					pCombobox->SetCurSel(0);
				}

				iRet = DM_ECODE_OK;
				DUIStatic* pStatic = pData->pPanel->FindChildByNameT<DUIStatic>(TREESTATICTEXT, true);
				if (!pStatic)
					break;
				pStatic->DM_EnableWindow(bEnable);
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnPreState0ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;

				bool bManualInvokeToShowPsData = false;
				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					bManualInvokeToShowPsData = true;
				}

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;

				HDMTREEITEM hNextItem = GetNextSiblingItem(hItem); DMASSERT(hNextItem);
				if (!hNextItem)
					break;
				iRet = EnableComboItem(hNextItem, pListBox->GetCurSel() != 0);//如果是NULL disable下一行的状态

				HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
				TransitionTreeDataPtr pParentData = (TransitionTreeDataPtr)GetItemData(hParentItem); DMASSERT(pParentData && pParentData->m_pJSonParser);
				if (!pParentData || !pParentData->m_pJSonParser)
					break;

				EDConditionsNodeParser* pCdtNodePs = dynamic_cast<EDConditionsNodeParser*>(pParentData->m_pJSonParser); DMASSERT(pCdtNodePs);
				if (pListBox->GetCurSel() == 0)
				{
					if (!bManualInvokeToShowPsData)
						pCdtNodePs->SetJSonAttributeString(EDAttr::EDConditionsNodeParserAttr::STRING_preState, L"");
				}
				else
				{
					CStringW strText;
					pListBox->GetText(pListBox->GetCurSel(), strText);
					DUIComboBox* pCombobox = GetTreeItemCombobox(hNextItem); DMASSERT(pCombobox);
					if (pCombobox)
					{
						if (!bManualInvokeToShowPsData)
							pCdtNodePs->SetJSonAttributeString(EDAttr::EDConditionsNodeParserAttr::STRING_preState, strText + CStringW(L".") + g_StateComboText[pCombobox->GetCurSel()].JStext);
					}
				}
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnPreState1ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					break;
				}

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;

				HDMTREEITEM hPrevItem = GetPrevSiblingItem(hItem); DMASSERT(hPrevItem);
				if (!hPrevItem)
					break;

				DUIComboBox* pCombobox = GetTreeItemCombobox(hPrevItem); DMASSERT(pCombobox);
				if (pCombobox)
				{
					DMEventLBSelChangedArgs EvtSelChanged(pCombobox->GetListBox());
					EvtSelChanged.m_nOldSel = 0;
					EvtSelChanged.m_nNewSel = pCombobox->GetListBox()->GetCurSel();
					pCombobox->GetListBox()->m_EventMgr.FireEvent(EvtSelChanged); //触发prestate的改变来达到设置json数据的要求
				}

				iRet = DM_ECODE_OK;
			} while (false);
			return DM_ECODE_OK;
		}

		DMCode DUITransitionTreeCtrl::OnEvent0ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;
				bool bManualInvokeTosSetPsData = false; //通过下面的项的combobox触发的   为了设置数据
				bool bManualInvokeToShowPsData = false;

				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nNewSel == pEvt->m_nOldSel)
					break;
				if (pEvt->m_nOldSel == ManualInvokeToSetPsData)
					bManualInvokeTosSetPsData = true;
				else if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
					bManualInvokeToShowPsData = true;

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);//第0项
				if (!hItem)
					break;

				HDMTREEITEM hNextItem = GetNextSiblingItem(hItem); DMASSERT(hNextItem);//第一项
				if (!hNextItem)
					break;

				DUIComboBox* pNextItemCombobox = GetTreeItemCombobox(hNextItem); DMASSERT(pNextItemCombobox);//第一项 的combobox
				if (!pNextItemCombobox)
					break;

				HDMTREEITEM hNextNextItem = GetNextSiblingItem(hNextItem); DMASSERT(hNextNextItem);//第二项
				if (!hNextNextItem)
					break;

				DUIComboBox* pNextNextItemCombobox = GetTreeItemCombobox(hNextNextItem);//第二项 的combobox
				if (!pNextNextItemCombobox)
					break;

				if (pListBox->GetCurSel() == ActionEvent && !bManualInvokeTosSetPsData && !bManualInvokeToShowPsData)//选择Action event
				{					
					pNextItemCombobox->ResetContent();
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Face].text);		pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Blink].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[OpenMouth].text);	pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[ShakeHead].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[NodHead].text);	pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[BrowJump].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Ok].text);			pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Scissor].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[ThumbUp].text);	pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Palm].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Pistol].text);		pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Love].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[HoldUp].text);		pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Congratulate].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[FingerHeart].text); pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[IndexFinger].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[HAND666].text);	pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[BLESS].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[ILOVEYOU].text);	pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[ForeGround].text);
					pNextItemCombobox->InsertItem(-1, g_ActionEventListComboText[Background].text);
					DUIWndAutoMuteGuard guard(pNextItemCombobox->GetListBox());
					pNextItemCombobox->SetCurSel(0);

					pNextNextItemCombobox->ResetContent();
					pNextNextItemCombobox->InsertItem(-1, EnumToStr(Appear));
					pNextNextItemCombobox->InsertItem(-1, EnumToStr(Disappear));
					DUIWndAutoMuteGuard guard2(pNextNextItemCombobox->GetListBox());
					pNextNextItemCombobox->SetCurSel(Appear);
				}
				else if (pListBox->GetCurSel() == StickerEvent && !bManualInvokeTosSetPsData && !bManualInvokeToShowPsData)//选择Sticker event
				{
					pNextItemCombobox->ResetContent();
					CArray<CStringW> ParserElementListArray;
					int iIndexList = 0;
					GetParserElementListArray(ParserElementListArray, L"", iIndexList);
					for (size_t i = 0; i < ParserElementListArray.GetCount(); i++)
					{
						pNextItemCombobox->InsertItem(-1, ParserElementListArray[i]);
					}
					DUIWndAutoMuteGuard guard(pNextItemCombobox->GetListBox());
					pNextItemCombobox->SetCurSel(0);

					pNextNextItemCombobox->ResetContent();
					pNextNextItemCombobox->InsertItem(-1, EnumToStr(Begin)); pNextNextItemCombobox->InsertItem(-1, EnumToStr(Play)); pNextNextItemCombobox->InsertItem(-1, EnumToStr(Pause));
					pNextNextItemCombobox->InsertItem(-1, EnumToStr(End));	 pNextNextItemCombobox->InsertItem(-1, EnumToStr(Hide));
					DUIWndAutoMuteGuard guard2(pNextNextItemCombobox->GetListBox());
					pNextNextItemCombobox->SetCurSel(Begin);
				}

				CStringW strTriggerState;
				strTriggerState = pListBox->GetCurSel() == ActionEvent ? L"H." : L"A.";
				if (pListBox->GetCurSel() == ActionEvent)
				{
					strTriggerState += g_ActionEventListComboText[pNextItemCombobox->GetCurSel()].JStext;
					strTriggerState += pNextNextItemCombobox->GetCurSel()==0 ? L".appear" : L".disappear";
				}
				else//sticker event
				{
					strTriggerState += pNextItemCombobox->GetCBText();
					strTriggerState += L".";
					strTriggerState += pNextNextItemCombobox->GetCBText().MakeLower();
				}

				//设置数据
				int iMeIndex = 0; //删除的event的index
				HDMTREEITEM hPrevItem = GetPrevSiblingItem(hItem);
				while (hPrevItem)
				{
					iMeIndex++;
					hPrevItem = GetPrevSiblingItem(hPrevItem);
				}
				int iEventIndex = (iMeIndex - 2) / 3;
				HDMTREEITEM hParentItem = GetParentItem(hItem);
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem); DMASSERT(pData && pData->m_pJSonParser);
				EDConditionsNodeParser* pCditNodePs = dynamic_cast<EDConditionsNodeParser*>(pData->m_pJSonParser); DMASSERT(pCditNodePs);
				if (!pCditNodePs)
					break;
				
				CArray<CStringW> ArrayTriggers;
				SplitStringT(pCditNodePs->m_strTriggers, L'&', ArrayTriggers);
				pCditNodePs->m_strTriggers.Empty();
				for (size_t i = 0; i < ArrayTriggers.GetCount(); i++)
				{
					if (!pCditNodePs->m_strTriggers.IsEmpty())
					{
						pCditNodePs->m_strTriggers += L"&";
					}
					if (iEventIndex == i)//当前event
					{
						pCditNodePs->m_strTriggers += strTriggerState;
						continue;
					}
					pCditNodePs->m_strTriggers += ArrayTriggers[i];
				}
				if (pCditNodePs->m_strTriggers.IsEmpty())
				{
					pCditNodePs->m_strTriggers = strTriggerState;
				}
				if (!bManualInvokeToShowPsData)
					pCditNodePs->MarkDataDirty();
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnEvent1ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nNewSel == pEvt->m_nOldSel)
					break;
								
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					break;
				}

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;

				HDMTREEITEM hPrevItem = GetPrevSiblingItem(hItem); DMASSERT(hPrevItem);//Event 第一项
				if (!hPrevItem)
					break;

				DUIComboBox* pCombobox = GetTreeItemCombobox(hPrevItem); DMASSERT(pCombobox);
				if (pCombobox)
				{
					DMEventLBSelChangedArgs EvtSelChanged(pCombobox->GetListBox());
					EvtSelChanged.m_nOldSel = ManualInvokeToSetPsData;//标记自己触发的
					EvtSelChanged.m_nNewSel = pCombobox->GetListBox()->GetCurSel();
					pCombobox->GetListBox()->m_EventMgr.FireEvent(EvtSelChanged); //触发prestate的改变来达到设置json数据的要求
				}
				iRet = DM_ECODE_OK;
			} while (false);
			return DM_ECODE_OK;
		}

		DMCode DUITransitionTreeCtrl::OnEvent2ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nNewSel == pEvt->m_nOldSel)
					break;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
					break;
				
				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;

				HDMTREEITEM hPrevItem = GetPrevSiblingItem(hItem); DMASSERT(hPrevItem);//Event 第二项
				if (!hPrevItem)
					break;

				HDMTREEITEM hPrevPrevItem = GetPrevSiblingItem(hPrevItem); DMASSERT(hPrevPrevItem);//Event 第一项
				if (!hPrevPrevItem)
					break;

				DUIComboBox* pCombobox = GetTreeItemCombobox(hPrevPrevItem); DMASSERT(pCombobox);
				if (pCombobox)
				{
					DMEventLBSelChangedArgs EvtSelChanged(pCombobox->GetListBox());
					EvtSelChanged.m_nOldSel = ManualInvokeToSetPsData;//标记自己触发的
					EvtSelChanged.m_nNewSel = pCombobox->GetListBox()->GetCurSel();
					pCombobox->GetListBox()->m_EventMgr.FireEvent(EvtSelChanged); //触发prestate的改变来达到设置json数据的要求
				}

	/*			DM::LPTVITEMEX pData = GetItem(hPrevPrevItem);
				if (!pData)
					break;

				DUIComboBox* pEvent0Combobox = pData->pPanel->FindChildByNameT<DUIComboBox>(COMBOBOXNAME, true); DMASSERT(pEvent0Combobox);
				if (!pEvent0Combobox)
					break;

				if (pEvent0Combobox->GetCurSel() == ActionEvent)//选中Action event
				{
					int iCurSelect = pListBox->GetCurSel();
					switch (iCurSelect)
					{
					case Appear:
						break;
					case Disappear:
						break;
					}
				}
				else if (pEvent0Combobox->GetCurSel() == StickerEvent)//选中Sticker event
				{
					int iCurSelect = pListBox->GetCurSel();
					switch (iCurSelect)
					{
					case Begin:
						break;
					case Play:
						break;
					case Pause:
						break;
					case End:
						break;
					case Hide:
						break;
					}
				}*/
			} while (false);
			return DM_ECODE_OK;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_TargetSticker_ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (!pEvt)
					break;

				bool bManualInvokeToShowPsData = false;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					bManualInvokeToShowPsData = true;
				}

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;
				
				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;

				HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
				EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
				if (!pTargetNodePs)
					break;
				
				CStringW strTargetPart;
				pListBox->GetText(pListBox->GetCurSel(), strTargetPart/*pTargetNodePs->m_strTargetPart*/);
				if (!bManualInvokeToShowPsData)
					pTargetNodePs->SetJSonAttributeString(EDAttr::EDTargetsNodeParserAttr::STRING_targetPart, strTargetPart);
				else
				{
					if (pTargetNodePs->m_strTargetPart.IsEmpty()) //new 一个target 没有初始化TargetPart
					{
						pTargetNodePs->SetJSonAttributeString(EDAttr::EDTargetsNodeParserAttr::STRING_targetPart, strTargetPart);
					}
				}
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_TargetState_ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				bool bManualInvokeToShowPsData = false;
				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					bManualInvokeToShowPsData = true;
				}

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;

				HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
				EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
				if (!pTargetNodePs)
					break;

				if (!bManualInvokeToShowPsData)
					pTargetNodePs->SetJSonAttributeString(EDAttr::EDTargetsNodeParserAttr::STRING_targetState, g_StateComboText[pListBox->GetCurSel()].JStext);

				HDMTREEITEM hDelayFrameItem = GetNextSiblingItem(hItem); DMASSERT(hDelayFrameItem);//DelayFrame项
				if (!hDelayFrameItem)
					break;
				HDMTREEITEM hFadeFrameItem = GetNextSiblingItem(hDelayFrameItem); DMASSERT(hFadeFrameItem);//FadeFrame项
				if (!hFadeFrameItem)
					break;
				HDMTREEITEM hLastingFrameItem = GetNextSiblingItem(hFadeFrameItem); DMASSERT(hLastingFrameItem);//hLastingFrame项
				if (!hLastingFrameItem)
					break;
				HDMTREEITEM hPlayLoopItem = GetNextSiblingItem(hLastingFrameItem); DMASSERT(hPlayLoopItem);//hPlayLoop项
				if (!hPlayLoopItem)
					break;

				if (pListBox->GetCurSel() == PausedInTheFirstFrame)//选择Paused in the First frame
				{
					EnableComboItem(hDelayFrameItem, TRUE);
					EnableComboItem(hFadeFrameItem, TRUE);
					EnableComboItem(hLastingFrameItem, TRUE);
					EnableComboItem(hPlayLoopItem, FALSE, TRUE);

					if (!bManualInvokeToShowPsData)
						pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_loop, 0);
				}
				else if (pListBox->GetCurSel() == Playing)//选择Playing
				{
					EnableComboItem(hDelayFrameItem, TRUE);
					EnableComboItem(hFadeFrameItem, TRUE);
					EnableComboItem(hLastingFrameItem, FALSE, TRUE);
					EnableComboItem(hPlayLoopItem, TRUE);
					if (!bManualInvokeToShowPsData)
						pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_lastingFrame, 0);
				}
				else if (pListBox->GetCurSel() == Paused)//选择Paused
				{
					EnableComboItem(hDelayFrameItem, TRUE);
					EnableComboItem(hFadeFrameItem, FALSE, TRUE);
					EnableComboItem(hLastingFrameItem, TRUE);
					EnableComboItem(hPlayLoopItem, FALSE, TRUE);
					if (!bManualInvokeToShowPsData)
					{
						pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_fadingFrame, 0);
						pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_loop, 0);
					}
				}
				else if (pListBox->GetCurSel() == PausedInTheLastFrame)//选择Paused In the last frame
				{
					EnableComboItem(hDelayFrameItem, TRUE);
					EnableComboItem(hFadeFrameItem, FALSE, TRUE);
					EnableComboItem(hLastingFrameItem, TRUE);
					EnableComboItem(hPlayLoopItem, FALSE, TRUE);
					if (!bManualInvokeToShowPsData)
					{
						pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_fadingFrame, 0);
						pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_loop, 0);
					}
				}
				else if (pListBox->GetCurSel() == Invisible)//选择Invisible
				{
					EnableComboItem(hDelayFrameItem, TRUE);
					EnableComboItem(hFadeFrameItem, TRUE);
					EnableComboItem(hLastingFrameItem, FALSE, TRUE);
					EnableComboItem(hPlayLoopItem, FALSE, TRUE);
					if (!bManualInvokeToShowPsData)
					{
						pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_lastingFrame, 0);
						pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_loop, 0);
					}
				}
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_DelayFrame_ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				bool bManualInvokeToShowPsData = false;
				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					bManualInvokeToShowPsData = true;
				}
				if (pEvt->m_nNewSel < 0)
					break;

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;

				CStringW strText;
				pListBox->GetText(pListBox->GetCurSel(), strText);
				m_iDelayFrame = _wtoi((LPCWSTR)strText);

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;
				HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
				EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
				if (!pTargetNodePs)
					break;
				if (!bManualInvokeToShowPsData)
					pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_delay, m_iDelayFrame);
			} while (false);
			return DM_ECODE_OK;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_EditInputEnter(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
				if (!pEdit)
					break;

				DUIComboBox* pComboBox = dynamic_cast<DUIComboBox*>(pEdit->DM_GetWindow(GDW_OWNER));
				if (!pComboBox)
					break;

				CStringW strText = pComboBox->m_pEdit->GetWindowTextW();

				bool bExist = false;
				for (int i = 0; i < pComboBox->GetCount(); i++)
				{
					CStringW strTmp = pComboBox->GetLBText(i);
					if (strTmp == strText)
					{
						bExist = true;
						break;
					}
				}
				if (!bExist)
					pComboBox->InsertItem(-1, strText);
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_DelayFrame_EditChanged(DMEventArgs* pEvt)
		{
			DMCode iRet = DM_ECODE_FAIL;
			DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
			do
			{
				if (EN_CHANGE != pEvent->m_iNotify)
					break;

				if (!pEvent)
					break;

				DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
				if (!pEdit)
					break;

				CStringW strText = pEdit->GetWindowTextW();
				int iDelayFrame = _wtoi((LPCWSTR)strText);
				if (iDelayFrame < 0 || iDelayFrame > 100000000)
				{
					ED_MessageBox(L"The effective range is [0, 100000000]", MB_OK, L"Error", g_pMainWnd->m_hWnd);
					CStringW strTmp;
					strTmp.Format(L"%d", m_iDelayFrame);
					pEdit->SetWindowTextW(strTmp);
				}
				else
				{
					m_iDelayFrame = iDelayFrame;

					DUIWindow* pParent = pEdit->DM_GetWindow(GDW_OWNER);//combobox
					if (!pParent)
						break;
					pParent = pParent->DM_GetWindow(GDW_PARENT);
					DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
					if (!pPanel)
						break;
					HDMTREEITEM hItem = (HDMTREEITEM)pPanel->GetItemId();

					HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
					TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
					EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
					if (!pTargetNodePs)
						break;
					pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_delay, m_iDelayFrame);
				}
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_FadeFrame_ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				bool bManualInvokeToShowPsData = false;
				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					bManualInvokeToShowPsData = true;
				}
				if (pEvt->m_nNewSel < 0)
					break;

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;
				CStringW strText;
				pListBox->GetText(pListBox->GetCurSel(), strText);
				m_iFadeFrame = _wtoi((LPCWSTR)strText);

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;
				HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
				EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
				if (!pTargetNodePs)
					break;

				if (!bManualInvokeToShowPsData)
					pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_fadingFrame, m_iFadeFrame);
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_FadeFrame_EditChanged(DMEventArgs* pEvt)
		{
			DMCode iRet = DM_ECODE_FAIL;
			DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
			do
			{
				if (EN_CHANGE != pEvent->m_iNotify)
					break;
				if (!pEvent)
					break;
				DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
				if (!pEdit)
					break;
				CStringW strText = pEdit->GetWindowTextW();
				int iFadeFrame = _wtoi((LPCWSTR)strText);
				if (iFadeFrame < 0 || iFadeFrame > 100000000)
				{
					ED_MessageBox(L"The effective range is [0, 100000000]", MB_OK, L"Error", g_pMainWnd->m_hWnd);
					CStringW strTmp;
					strTmp.Format(L"%d", m_iFadeFrame);
					pEdit->SetWindowTextW(strTmp);
				}
				else
				{
					m_iFadeFrame = iFadeFrame;

					DUIWindow* pParent = pEdit->DM_GetWindow(GDW_OWNER);//combobox
					if (!pParent)
						break;
					pParent = pParent->DM_GetWindow(GDW_PARENT);
					DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
					if (!pPanel)
						break;
					HDMTREEITEM hItem = (HDMTREEITEM)pPanel->GetItemId();

					HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
					TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
					EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
					if (!pTargetNodePs)
						break;
					pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_fadingFrame, m_iFadeFrame);
				}
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_LastingFrame_ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				bool bManualInvokeToShowPsData = false;
				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					bManualInvokeToShowPsData = true;
				}
				if (pEvt->m_nNewSel < 0)
					break;

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;

				CStringW strText;
				pListBox->GetText(pListBox->GetCurSel(), strText);
				m_iLastingFrame = _wtoi((LPCWSTR)strText);

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;
				HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
				EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
				if (!pTargetNodePs)
					break;
				if (!bManualInvokeToShowPsData)
					pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_lastingFrame, m_iLastingFrame);
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_LastingFrame_EditChanged(DMEventArgs* pEvt)
		{
			DMCode iRet = DM_ECODE_FAIL;
			DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
			do
			{
				if (EN_CHANGE != pEvent->m_iNotify)
					break;
				if (!pEvent)
					break;
				DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
				if (!pEdit)
					break;
				CStringW strText = pEdit->GetWindowTextW();
				int iLastingFrame = _wtoi((LPCWSTR)strText);
				if (iLastingFrame < 0 || iLastingFrame > 100000000)
				{
					ED_MessageBox(L"The effective range is [0, 100000000]", MB_OK, L"Error", g_pMainWnd->m_hWnd);
					CStringW strTmp;
					strTmp.Format(L"%d", m_iLastingFrame);
					pEdit->SetWindowTextW(strTmp);
				}
				else
				{
					m_iLastingFrame = iLastingFrame;

					DUIWindow* pParent = pEdit->DM_GetWindow(GDW_OWNER);//combobox
					if (!pParent)
						break;
					pParent = pParent->DM_GetWindow(GDW_PARENT);
					DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
					if (!pPanel)
						break;
					HDMTREEITEM hItem = (HDMTREEITEM)pPanel->GetItemId();

					HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
					TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
					EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
					if (!pTargetNodePs)
						break;
					pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_lastingFrame, m_iLastingFrame);
				}
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_PlayLoop_ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				bool bManualInvokeToShowPsData = false;
				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					bManualInvokeToShowPsData = true;
				}
				if (pEvt->m_nNewSel < 0)
					break;

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;

				CStringW strText;
				pListBox->GetText(pListBox->GetCurSel(), strText);
				m_iPlayLoop = _wtoi((LPCWSTR)strText);

				HDMTREEITEM hItem = ComboSeleChgTreeItem(pEvent);
				if (!hItem)
					break;
				HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
				EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
				if (!pTargetNodePs)
					break;
				if (!bManualInvokeToShowPsData)
					pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_loop, m_iPlayLoop);
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTarget_PlayLoop_EditChanged(DMEventArgs* pEvt)
		{
			DMCode iRet = DM_ECODE_FAIL;
			DMEventRENotifyArgs *pEvent = (DMEventRENotifyArgs*)(pEvt);
			do
			{
				if (EN_CHANGE != pEvent->m_iNotify)
					break;
				if (!pEvent)
					break;
				DUIRichEdit* pEdit = dynamic_cast<DUIRichEdit*>(pEvent->m_pSender);
				if (!pEdit)
					break;
				CStringW strText = pEdit->GetWindowTextW();
				int iPlayLoop = _wtoi((LPCWSTR)strText);
				if (iPlayLoop < 0 || iPlayLoop > 100000000)
				{
					ED_MessageBox(L"The effective range is [0, 100000000]", MB_OK, L"Error", g_pMainWnd->m_hWnd);
					CStringW strTmp;
					strTmp.Format(L"%d", m_iPlayLoop);
					pEdit->SetWindowTextW(strTmp);
				}
				else
				{
					m_iPlayLoop = iPlayLoop;

					DUIWindow* pParent = pEdit->DM_GetWindow(GDW_OWNER);//combobox
					if (!pParent)
						break;
					pParent = pParent->DM_GetWindow(GDW_PARENT);
					DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
					if (!pPanel)
						break;
					HDMTREEITEM hItem = (HDMTREEITEM)pPanel->GetItemId();

					HDMTREEITEM hParentItem = GetParentItem(hItem); DMASSERT(hParentItem);
					TransitionTreeDataPtr pData = (TransitionTreeDataPtr)GetItemData(hParentItem);
					EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser); DMASSERT(pTargetNodePs);
					if (!pTargetNodePs)
						break;
					pTargetNodePs->SetJSonAttributeInt(EDAttr::EDTargetsNodeParserAttr::INT_loop, m_iPlayLoop);
				}
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnRandowShow_ComboSelectChanged(DMEventArgs* pEvent)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				if (!pEvent)
					break;

				bool bManualInvokeToShowPsData = false;
				DMEventLBSelChangedArgs * pEvt = (DMEventLBSelChangedArgs*)pEvent;
				if (pEvt->m_nOldSel == ManualInvokeToShowPsData)
				{
					bManualInvokeToShowPsData = true;
				}

				DUIListBox* pListBox = dynamic_cast<DUIListBox*>(pEvent->m_pSender);
				if (!pListBox)
					break;

				EDTransitionsNodeParser* pTransitionNodePs = dynamic_cast<EDTransitionsNodeParser*>(m_pCurShowJsonParser);
				if (!pTransitionNodePs)
					break;

				if (!bManualInvokeToShowPsData)
					pTransitionNodePs->SetJSonAttributeBool(EDAttr::EDTransitionsNodeParserAttr::BOOL_randomShow, pListBox->GetCurSel() == 0 ? true : false);
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::OnTreeItemEditKillFocus(DMEventArgs* pEvent)
		{
			DMCode iErr = DM_ECODE_FAIL;
			do
			{
				DMEventRENotifyArgs *pEvt = (DMEventRENotifyArgs*)pEvent;
				if (!pEvt)
					break;
				static bool bRetry = false;
				if (EN_KILLFOCUS == pEvt->m_iNotify && false == bRetry)
				{
					DUIRichEdit* pTreeItemEdit = (DUIRichEdit*)pEvt->m_pSender;
					if (!pTreeItemEdit)
						break;
					DUIWindow* pParent = pTreeItemEdit->DM_GetWindow(GDW_PARENT);
					DUIItemPanel* pPanel = dynamic_cast<DUIItemPanel*>(pParent);
					if (!pPanel)
						break;

					DUIStatic* pStatic = pPanel->FindChildByNameT<DUIStatic>(TREESTATICTEXT);
					CStringW strInfo = pTreeItemEdit->GetWindowText();
					if (!strInfo.IsEmpty() && pStatic)
					{
						CStringW src = pStatic->m_pDUIXmlInfo->m_strText;
						if (strInfo.Compare((LPCTSTR)src) != 0)
						{
							std::wstring tmpDes((LPCTSTR)strInfo);
							//发rename回调

							HDMTREEITEM hItemRenamed = NULL;
							if (NULL != m_hOldSelectItem && DMTVI_ROOT != m_hOldSelectItem)
							{
								DM::LPTVITEMEX data = GetItem(m_hOldSelectItem);
								if (data->pPanel == pPanel)
								{
									hItemRenamed = m_hOldSelectItem;
								}
							}
							if (hItemRenamed == NULL && NULL != m_hSelItem && DMTVI_ROOT != m_hSelItem)
							{
								DM::LPTVITEMEX data = GetItem(m_hSelItem);
								if (data->pPanel == pPanel)
								{
									hItemRenamed = m_hSelItem;
								}
							}

							if (hItemRenamed)
							{
								DMASSERT_EXPR(FALSE, L"rename");
							}

							if (!tmpDes.empty())
								pStatic->DV_SetWindowText(tmpDes.c_str());
						}
					}
					bRetry = true;
					pTreeItemEdit->DM_SetVisible(false, true);// 此函数会引发多次进入EN_KILLFOCUS，所以加判断

					if (pStatic)
					{
						pStatic->DM_SetVisible(true, true);
					}
					bRetry = false;
					iErr = DM_ECODE_OK;
				}
			} while (false);
			return iErr;
		}

		DMCode DUITransitionTreeCtrl::OnTreeItemEditInputReturn(DMEventArgs* pEvent)
		{
			DUIRichEdit* pTreeItemEdit = (DUIRichEdit*)pEvent->m_pSender;
			if (!pTreeItemEdit)
				return DM_ECODE_FAIL;
			if (pTreeItemEdit) pTreeItemEdit->DM_OnKillFocus();
			return DM_ECODE_OK;
		}

		DMCode DUITransitionTreeCtrl::OnTreeItemSelectChanged(DMEventArgs* pEvet)
		{
			DMCode iRet = DM_ECODE_FAIL;
			do
			{
				DM::DMEventTCSelChangedArgs *pEvent = (DM::DMEventTCSelChangedArgs*)pEvet;
				if (!pEvent || !pEvent->m_hOldSel || DMTVI_ROOT == pEvent->m_hOldSel)
					break;
				m_hOldSelectItem = pEvent->m_hOldSel;
				DM::LPTVITEMEX pData = GetItem(pEvent->m_hOldSel);
				if (!pData)
					break;
				DUIRichEdit* pItemEdit = pData->pPanel->FindChildByNameT<DUIRichEdit>(L"treeitemedit", true);
				if (!pItemEdit)
					break;
				pItemEdit->DM_OnKillFocus();
				DV_SetFocusWnd();
				iRet = DM_ECODE_OK;
			} while (false);
			return iRet;
		}

		DMCode DUITransitionTreeCtrl::RenameTreeItem(HDMTREEITEM hItem)
		{
			DMCode hRet = DM_ECODE_FAIL;
			do
			{
				if (NULL == hItem || DMTVI_ROOT == hItem)
				{
					break;
				}

				DM::LPTVITEMEX pData = GetItem(hItem);
				if (!pData)
					break;

				DUIStatic* pStaticText = pData->pPanel->FindChildByNameT<DUIStatic>(TREESTATICTEXT, true); DMASSERT(pStaticText);
				DUIRichEdit* pItemEdit = pData->pPanel->FindChildByNameT<DUIRichEdit>(L"treeitemedit", true); DMASSERT(pItemEdit);
				if (!pStaticText || !pItemEdit)
				{
					break;
				}

				pItemEdit->SetAttribute(L"text", pStaticText->m_pDUIXmlInfo->m_strText);
				pItemEdit->DM_SetVisible(true, true);
				pStaticText->DM_SetVisible(false, true);

				pItemEdit->m_EventMgr.SubscribeEvent(DM::DMEventRENotifyArgs::EventID, Subscriber(&DUITransitionTreeCtrl::OnTreeItemEditKillFocus, this));
				pItemEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, Subscriber(&DUITransitionTreeCtrl::OnTreeItemEditInputReturn, this));
				hRet = DM_ECODE_OK;
			} while (false);
			return hRet;
		}

		DMCode DUITransitionTreeCtrl::RenameTreeItemWithNoEdit(HDMTREEITEM hItem, CStringW strName)
		{
			DMCode hRet = DM_ECODE_FAIL;
			do
			{
				if (NULL == hItem || DMTVI_ROOT == hItem)
				{
					break;
				}

				DM::LPTVITEMEX pData = GetItem(hItem);
				if (!pData)
					break;

				DUIStatic* pStaticText = pData->pPanel->FindChildByNameT<DUIStatic>(TREESTATICTEXT, true); DMASSERT(pStaticText);
				if (!pStaticText)
				{
					break;
				}
				pStaticText->DV_SetWindowText(strName);
				hRet = DM_ECODE_OK;
			} while (false);
			return hRet;
		}

		CStringW DUITransitionTreeCtrl::GetTreeItemName(HDMTREEITEM hItem)
		{
			CStringW strRet;
			do
			{
				if (NULL == hItem || DMTVI_ROOT == hItem)
				{
					break;
				}

				DM::LPTVITEMEX pData = GetItem(hItem);
				if (!pData)
					break;

				DUIStatic* pStaticText = pData->pPanel->FindChildByNameT<DUIStatic>(TREESTATICTEXT, true); DMASSERT(pStaticText);
				if (!pStaticText)
				{
					break;
				}
				strRet = pStaticText->m_pDUIXmlInfo->m_strText;
			} while (false);
			return strRet;
		}

		DUIComboBox* DUITransitionTreeCtrl::GetTreeItemCombobox(HDMTREEITEM hItem)
		{
			DUIComboBox* pCombobox = NULL;
			do
			{
				if (NULL == hItem || DMTVI_ROOT == hItem)
					break;

				DM::LPTVITEMEX pData = GetItem(hItem);
				if (!pData)
					break;

				pCombobox = pData->pPanel->FindChildByNameT<DUIComboBox>(COMBOBOXNAME, true); DMASSERT(pCombobox);
			} while (false);
			return pCombobox;
		}

		bool DUITransitionTreeCtrl::HoverItem(HDMTREEITEM hItem, bool bEnsureVisible)
		{
			bool bRet = false;
			do
			{
				if (m_hHoverItem == hItem)
				{
					bRet = true;
					break;
				}

				if (NULL == hItem || DMTVI_ROOT == hItem)
				{
					break;
				}

				if (hItem&&bEnsureVisible)
				{
					EnsureVisible(hItem);
				}
				HDMTREEITEM hOldItem = m_hHoverItem;
				m_hHoverItem = hItem;
				if (hOldItem)
				{
					RedrawItem(hOldItem);
				}
				if (m_hHoverItem)
				{
					RedrawItem(m_hHoverItem);
				}

				bRet = true;
			} while (false);
			return bRet;
		}

		DMXmlNode DUITransitionTreeCtrl::InitTransitionItemXml(DMXmlNode &XmlItemIn, CStringW strTransitionName)
		{
			DMXmlNode XmlItem = XmlItemIn.InsertChildNode(L"treeitem");
			XmlItem.SetAttribute(L"bcollapsed", L"0"); XmlItem.SetAttribute(L"childoffset", L"10"); //XmlItem.SetAttributeInt(L"data", TagTransitionNode);
			DMXmlNode StaXmlItem = XmlItem.InsertChildNode(L"static");
			StaXmlItem.SetAttribute(L"name", TREESTATICTEXT); StaXmlItem.SetAttribute(L"pos", L"28,0,-65,-0"); StaXmlItem.SetAttribute(L"text", strTransitionName);
			StaXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,dd)"); StaXmlItem.SetAttribute(L"align", L"left"); StaXmlItem.SetAttribute(L"alignv", L"center");
			StaXmlItem.SetAttribute(L"bdot", L"1"); StaXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:150");
			DMXmlNode EditXmlItem = XmlItem.InsertChildNode(L"richedit");
			EditXmlItem.SetAttribute(L"name", L"treeitemedit"); EditXmlItem.SetAttribute(L"pos", L"28,4,-68,-4"); EditXmlItem.SetAttribute(L"skin", L"ds_editframe"); EditXmlItem.SetAttribute(L"maxbuf", L"100");
			EditXmlItem.SetAttribute(L"rcinsertmargin", L"2,1,2,0"); EditXmlItem.SetAttribute(L"clrcaret", L"pbgra(ff,ff,ff,ff)"); EditXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,ff)"); EditXmlItem.SetAttribute(L"bwantreturn", L"1");
			EditXmlItem.SetAttribute(L"bvisible", L"0"); EditXmlItem.SetAttribute(L"bautosel", L"1"); EditXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:200");
			DMXmlNode BtnXmlItem = XmlItem.InsertChildNode(L"button");
			BtnXmlItem.SetAttribute(L"name", L"treeaddbutton"); BtnXmlItem.SetAttribute(L"pos", L"-60,5,@16,@16"); BtnXmlItem.SetAttribute(L"cursor", L"hand"); BtnXmlItem.SetAttribute(L"skin", L"ds_treeaddeffectbutton");
			Init_Debug_XmlBuf(XmlItemIn);

			return XmlItem;
		}

		DMXmlNode DUITransitionTreeCtrl::InitConditionOrTargetItemXml(DMXmlNode &XmlItemIn, const CStringW& staShowName)
		{
			DMXmlNode XmlItem = XmlItemIn.InsertChildNode(L"treeitem");
			XmlItem.SetAttribute(L"bcollapsed", L"0"); XmlItem.SetAttribute(L"childoffset", L"14"); 
			DMXmlNode StaXmlItem = XmlItem.InsertChildNode(L"static");
			StaXmlItem.SetAttribute(L"name", TREESTATICTEXT); StaXmlItem.SetAttribute(L"pos", L"33,0,-65,-0"); StaXmlItem.SetAttribute(L"text", staShowName);
			StaXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,dd)"); StaXmlItem.SetAttribute(L"align", L"left"); StaXmlItem.SetAttribute(L"alignv", L"center");
			StaXmlItem.SetAttribute(L"bdot", L"1"); StaXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:150");
			DMXmlNode BtnXmlItem = XmlItem.InsertChildNode(L"button");
			BtnXmlItem.SetAttribute(L"name", L"treeaddbutton"); BtnXmlItem.SetAttribute(L"pos", L"-30,5,@16,@16"); BtnXmlItem.SetAttribute(L"cursor", L"hand"); BtnXmlItem.SetAttribute(L"skin", L"ds_treeaddeffectbutton");
			Init_Debug_XmlBuf(XmlItemIn);

			return XmlItem;
		}

		void DUITransitionTreeCtrl::GetParserElementListArray(CArray<CStringW>& ListArray, CStringW strItem, int& index)
		{
			EDJSonParser* pPartsJsonParser = g_pMainWnd->m_JSonMainParser.FindChildParserByClassName(EDPartsParser::GetClassNameW());
			if (pPartsJsonParser)
			{
				EDPartsNodeParser* pPartsNodeParser = dynamic_cast<EDPartsNodeParser*>(pPartsJsonParser->GetParser(GPS_FIRSTCHILD));
				while (pPartsNodeParser)
				{
					if (wcscmp(strItem, std::wstring(EDBASE::a2w(pPartsNodeParser->m_strJSonMemberKey)).c_str()) == 0)
					{
						index = ListArray.GetCount();
					}
					ListArray.Add(std::wstring(EDBASE::a2w(pPartsNodeParser->m_strJSonMemberKey)).c_str());
					pPartsNodeParser = dynamic_cast<EDPartsNodeParser*>(pPartsNodeParser->GetParser(GPS_NEXTSIBLING));
				}
			}

			EDJSonParser* pMakeupsJsonParser = g_pMainWnd->m_JSonMainParser.FindChildParserByClassName(EDMakeupsParser::GetClassNameW());
			if (pMakeupsJsonParser)
			{
				EDMakeupsNodeParser* pMakeupsNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pMakeupsJsonParser->GetParser(GPS_FIRSTCHILD));
				while (pMakeupsNodeParser)
				{
					if (wcscmp(strItem, pMakeupsNodeParser->m_strName) == 0)
					{
						index = ListArray.GetCount();
					}
					ListArray.Add(pMakeupsNodeParser->m_strName);
					pMakeupsNodeParser = dynamic_cast<EDMakeupsNodeParser*>(pMakeupsNodeParser->GetParser(GPS_NEXTSIBLING));
				}
			}
		}

		DMXmlNode DUITransitionTreeCtrl::InitConditionMajorXml(DMXmlNode &XmlItem, EDConditionsNodeParser* pJsonParser, CStringW strConditionName)
		{
			DMXmlNode CondXmlItem = InitConditionOrTargetItemXml(XmlItem, strConditionName);
			CondXmlItem.SetAttributeInt64(L"data", (INT64)(new TransitionTreeData(TagConditionNode, pJsonParser)));
			CondXmlItem.SetAttribute(L"bcollapsed", pJsonParser->m_bTreeItemCollapsed ? L"1" : L"0");

			//初始化preState
			CArray<CStringW> ArrayPreState;
			SplitStringT(pJsonParser->m_strPreState, L'.', ArrayPreState);
			CArray<CStringW> ParserElementListArray;
			ParserElementListArray.Add(L"NULL");
			int iIndexList = 0, iIndexState = 0;
			GetParserElementListArray(ParserElementListArray, ArrayPreState.GetCount() > 0 ? ArrayPreState[0] : L"", iIndexList);
			InitTreeItemNodeXml(CondXmlItem, L"PreState", Subscriber(&DUITransitionTreeCtrl::OnPreState0ComboSelectChanged, this).Clone(), iIndexList, ParserElementListArray);
			for (int index = 0; ArrayPreState.GetCount() > 1 && index <= Invisible; index ++)
			{
				if (_wcsicmp(g_StateComboText[index].JStext, ArrayPreState[1]) == 0)
				{
					iIndexState = index;
					break;
				}
			}
			InitTreeItemNodeXml(CondXmlItem, L"", Subscriber(&DUITransitionTreeCtrl::OnPreState1ComboSelectChanged, this).Clone(), iIndexState, g_StateComboText[PausedInTheFirstFrame].text, g_StateComboText[Playing].text, g_StateComboText[Paused].text, g_StateComboText[PausedInTheLastFrame].text, g_StateComboText[Invisible].text);

			//初始化event
			CArray<CStringW> ArrayEvent;
			int iEventCount = SplitStringT(pJsonParser->m_strTriggers, L'&', ArrayEvent);
			for (int i = 0; i < iEventCount; i++)
			{
				CStringW strEventName = L"Event";
				if (i > 0){
					strEventName.Format(L"Event%d", i);
				}
				InitEventMajorXml(CondXmlItem, ArrayEvent[i], strEventName);
			}			
			return CondXmlItem;
		}

		DMXmlNode DUITransitionTreeCtrl::InitEventMajorXml(DMXmlNode &XmlItem, CStringW strEventState, CStringW strEventName)
		{
			CArray<CStringW> ArrayEventSate;
			int iEventCount = SplitStringT(strEventState, L'.', ArrayEventSate);
			if (ArrayEventSate.GetCount() < 3)
			{
				DMASSERT(FALSE);
				return DMXmlNode();
			}

			DMXmlNode XmlNodeRet = InitTreeItemNodeXml(XmlItem, strEventName, Subscriber(&DUITransitionTreeCtrl::OnEvent0ComboSelectChanged, this).Clone(), ArrayEventSate[0] == L"H" ? 0 : 1, g_EventTypeComboText[ActionEvent].text, g_EventTypeComboText[StickerEvent].text);
			XmlNodeRet.SetAttributeInt64(L"data", (INT64)(new TransitionTreeData(TagConditionEventNode, NULL)));

			int iIndexEvent = 0;
			if (ArrayEventSate[0] == L"H") //action event
			{
				for (int i = 0; i < ActionEventListMaxIndex; i++)
				{
					if (_wcsicmp(g_ActionEventListComboText[i].JStext, ArrayEventSate[1]) == 0)
					{
						iIndexEvent = i;
						break;
					}
				}

				InitTreeItemNodeXml(XmlItem, L"", Subscriber(&DUITransitionTreeCtrl::OnEvent1ComboSelectChanged, this).Clone(), iIndexEvent,
					g_ActionEventListComboText[Face].text, g_ActionEventListComboText[Blink].text, g_ActionEventListComboText[OpenMouth].text, g_ActionEventListComboText[ShakeHead].text,
					g_ActionEventListComboText[NodHead].text, g_ActionEventListComboText[BrowJump].text, g_ActionEventListComboText[Ok].text, g_ActionEventListComboText[Scissor].text,
					g_ActionEventListComboText[ThumbUp].text, g_ActionEventListComboText[Palm].text, g_ActionEventListComboText[Pistol].text, g_ActionEventListComboText[Love].text, g_ActionEventListComboText[HoldUp].text,
					g_ActionEventListComboText[Congratulate].text, g_ActionEventListComboText[FingerHeart].text, g_ActionEventListComboText[IndexFinger].text, g_ActionEventListComboText[HAND666].text,
					g_ActionEventListComboText[BLESS].text, g_ActionEventListComboText[ILOVEYOU].text, g_ActionEventListComboText[ForeGround].text, g_ActionEventListComboText[Background].text);
		
				InitTreeItemNodeXml(XmlItem, L"", Subscriber(&DUITransitionTreeCtrl::OnEvent2ComboSelectChanged, this).Clone(), _wcsicmp(ArrayEventSate[2], EnumToStr(Appear)) == 0 ? 0 : 1, EnumToStr(Appear), EnumToStr(Disappear));
			}
			else//sticker event
			{
				CArray<CStringW> ParserElementListArray;
				int iIndexList = 0, iIndexState = 0;
				GetParserElementListArray(ParserElementListArray, ArrayEventSate[1], iIndexList);
				InitTreeItemNodeXml(XmlItem, L"", Subscriber(&DUITransitionTreeCtrl::OnEvent1ComboSelectChanged, this).Clone(), iIndexList, ParserElementListArray);

				//CStringW str = ArrayEventSate[2];
				if (_wcsicmp(EnumToStr(Begin), ArrayEventSate[2]) == 0)
				{
					iIndexState = Begin;
				}
				else if (_wcsicmp(EnumToStr(Play), ArrayEventSate[2]) == 0)
				{
					iIndexState = Play;
				}
				else if (_wcsicmp(EnumToStr(Pause), ArrayEventSate[2]) == 0)
				{
					iIndexState = Pause;
				}
				else if (_wcsicmp(EnumToStr(End), ArrayEventSate[2]) == 0)
				{
					iIndexState = End;
				}
				else if (_wcsicmp(EnumToStr(Hide), ArrayEventSate[2]) == 0)
				{
					iIndexState = Hide;
				}
				InitTreeItemNodeXml(XmlItem, L"", Subscriber(&DUITransitionTreeCtrl::OnEvent2ComboSelectChanged, this).Clone(), iIndexState, EnumToStr(Begin), EnumToStr(Play), EnumToStr(Pause), EnumToStr(End), EnumToStr(Hide));
			}
			
			return XmlNodeRet;
		}

		DMXmlNode DUITransitionTreeCtrl::InitTargetMajorXml(DMXmlNode &XmlItem, EDTargetsNodeParser* pJsonParser, CStringW strTargetName)
		{
			DMXmlNode TargetXmlItem = InitConditionOrTargetItemXml(XmlItem, strTargetName);
			TargetXmlItem.SetAttributeInt64(L"data", (INT64)(new TransitionTreeData(TagTargetNode, pJsonParser)));
			TargetXmlItem.SetAttribute(L"bcollapsed", pJsonParser->m_bTreeItemCollapsed ? L"1" : L"0");

			CArray<CStringW> ParserElementListArray;
			int iIndexList = 0, iIndexState = 0;
			GetParserElementListArray(ParserElementListArray, pJsonParser->m_strTargetPart, iIndexList);
			InitTreeItemNodeXml(TargetXmlItem, L"TargetSticker", Subscriber(&DUITransitionTreeCtrl::OnTarget_TargetSticker_ComboSelectChanged, this).Clone(), iIndexList, ParserElementListArray);
			
			for (int i = 0; i <= Invisible; i++)
			{
				if (_wcsicmp(g_StateComboText[i].JStext, pJsonParser->m_strTargetState) == 0)
				{
					iIndexState = i;
					break;
				}
			}
			InitTreeItemNodeXml(TargetXmlItem, L"TargetState", Subscriber(&DUITransitionTreeCtrl::OnTarget_TargetState_ComboSelectChanged, this).Clone(), iIndexState, g_StateComboText[PausedInTheFirstFrame].text, g_StateComboText[Playing].text, g_StateComboText[Paused].text, g_StateComboText[PausedInTheLastFrame].text, g_StateComboText[Invisible].text);
		
			InitTreeItemWithComboEditNodeXml(TargetXmlItem, L"DelayFrame", Subscriber(&DUITransitionTreeCtrl::OnTarget_DelayFrame_ComboSelectChanged, this).Clone(),
				Subscriber(&DUITransitionTreeCtrl::OnTarget_EditInputEnter, this).Clone(), Subscriber(&DUITransitionTreeCtrl::OnTarget_DelayFrame_EditChanged, this).Clone(), std::to_wstring(pJsonParser->m_iDelay).c_str(), L"0", L"20", L"40"),
			InitTreeItemWithComboEditNodeXml(TargetXmlItem, L"FadeFrame", Subscriber(&DUITransitionTreeCtrl::OnTarget_FadeFrame_ComboSelectChanged, this).Clone(),
				Subscriber(&DUITransitionTreeCtrl::OnTarget_EditInputEnter, this).Clone(), Subscriber(&DUITransitionTreeCtrl::OnTarget_FadeFrame_EditChanged, this).Clone(), std::to_wstring(pJsonParser->m_iFadingFrame).c_str(), L"0", L"20", L"40");
			InitTreeItemWithComboEditNodeXml(TargetXmlItem, L"LastingFrame", Subscriber(&DUITransitionTreeCtrl::OnTarget_LastingFrame_ComboSelectChanged, this).Clone(),
				Subscriber(&DUITransitionTreeCtrl::OnTarget_EditInputEnter, this).Clone(), Subscriber(&DUITransitionTreeCtrl::OnTarget_LastingFrame_EditChanged, this).Clone(), std::to_wstring(pJsonParser->m_iLastingFrame).c_str(), L"0", L"20", L"40");
			InitTreeItemWithComboEditNodeXml(TargetXmlItem, L"PlayLoop", Subscriber(&DUITransitionTreeCtrl::OnTarget_PlayLoop_ComboSelectChanged, this).Clone(),
				Subscriber(&DUITransitionTreeCtrl::OnTarget_EditInputEnter, this).Clone(), Subscriber(&DUITransitionTreeCtrl::OnTarget_PlayLoop_EditChanged, this).Clone(), std::to_wstring(pJsonParser->m_iLoop).c_str(), L"0", L"1", L"2", L"3", L"4");
			return TargetXmlItem;
		}

		DMXmlNode DUITransitionTreeCtrl::InitRandomShowNodeXml(DMXmlNode &XmlItem, EDTransitionsNodeParser* pJsonParser /*= NULL*/, CStringW strRandowShowName /*= L"RandomShow"*/)
		{
			DMXmlNode RandomShowXml = InitTreeItemNodeXml(XmlItem, strRandowShowName, Subscriber(&DUITransitionTreeCtrl::OnRandowShow_ComboSelectChanged, this).Clone(), pJsonParser && pJsonParser->m_bRandomShow ? 0 : 1, L"Yes", L"No");
			if (RandomShowXml.IsValid())
			{
				DMXmlNode StaticXml = RandomShowXml.FirstChild(L"static");
				if (StaticXml.IsValid())
				{
					StaticXml.SetAttribute(L"pos", L"33,0,@80,-0");
				}
			}
			return RandomShowXml;
		}

		HDMTREEITEM DUITransitionTreeCtrl::InsertItem(DMXmlNode &XmlItem, HDMTREEITEM hParent/*=DMTVI_ROOT*/, HDMTREEITEM hInsertAfter/*=DMTVI_LAST*/, bool bEnsureVisible/*=false*/)
		{
			HDMTREEITEM hItem = (HDMTREEITEM)NULL;
			do
			{
				hItem = DUITreeCtrlEx::InsertItem(XmlItem, hParent, hInsertAfter, bEnsureVisible); DMASSERT(hItem);
				if (NULL == hItem
					|| DMTVI_ROOT == hItem)
				{
					break;
				}
				DM::LPTVITEMEX pData = GetItem(hItem);
				if (!pData)
					break;
				DUIButton* pTreeAddBtn = pData->pPanel->FindChildByNameT<DUIButton>(L"treeaddbutton", true);
				if (pTreeAddBtn)
				{
					pTreeAddBtn->m_EventMgr.SubscribeEvent(DMEventCmdArgs::EventID, Subscriber(&DUITransitionTreeCtrl::OnTreeAddBtnClicked, this));
				}
			} while (false);

			UpdateScrollRange();
			return hItem;
		}

		void DUITransitionTreeCtrl::OnLButtonDown(UINT nFlags, CPoint pt)
		{
			do
			{
				HDMTREEITEM hItem = HitTest(pt);
				if (NULL == hItem || DMTVI_ROOT == hItem)
				{
					if (NULL == hItem && GetSelectedItem())
					{
						//INSERTNEWOBSERVEACTION(new TreeSelectActionSlot(m_hSelItem, NULL, this));
						SelectItem(NULL); //lzlong  取消选择
					}
					break;
				}

				DM::LPTVITEMEX pData = GetItem(hItem);
				DUIButton* pTreeAddBtn = pData->pPanel->FindChildByNameT<DUIButton>(L"treeaddbutton", true);
				if (pTreeAddBtn && pTreeAddBtn->HitTestPoint(pt))
				{
					SetMsgHandled(TRUE);//不需要传递下去了
					break;
				}

				m_hHoverItem = hItem;
				if (m_hHoverItem != m_hSelItem && m_hHoverItem)
				{
					//INSERTNEWOBSERVEACTION(new TreeSelectActionSlot(m_hSelItem, m_hHoverItem, this));
					SelectItem(m_hHoverItem, false);
				}

				if (m_hHoverItem)
				{
					m_hCaptureItem = m_hHoverItem;
					ItemLButtonDown(m_hHoverItem, nFlags, pt);
				}
				SetMsgHandled(FALSE);
			} while (false);
		}

		void DUITransitionTreeCtrl::OnLButtonDbClick(UINT nFlags, CPoint pt)
		{
			do
			{
				HDMTREEITEM hItem = HitTest(pt);
				if (NULL == hItem || DMTVI_ROOT == hItem)
				{
					break;
				}

				DM::LPTVITEMEX pData = GetItem(hItem);
				DUIButton* pTreeAddBtn = pData->pPanel->FindChildByNameT<DUIButton>(L"treeaddbutton", true);
				if (pTreeAddBtn && pTreeAddBtn->HitTestPoint(pt))
					break;

				DUIRichEdit* pItemEdit = pData->pPanel->FindChildByNameT<DUIRichEdit>(L"treeitemedit", true);
				if (pItemEdit && pItemEdit->HitTestPoint(pt))
				{
					RenameTreeItem(hItem);
				}
				m_hHoverItem = hItem;
				if (m_hHoverItem)
				{
					Expand(m_hHoverItem, DMTVEX_TOGGLE);
					DUITreeCtrlEx::ItemLButtonDbClick(m_hHoverItem, nFlags, pt);
				}
			} while (false);
			SetMsgHandled(TRUE);
		}

		HDMTREEITEM DUITransitionTreeCtrl::GetNextVisibleItem(HDMTREEITEM hItem)
		{
			if (hItem == DMTVI_ROOT)
			{
				return (HDMTREEITEM)m_hRootFirst;
			}

			if (NULL == hItem)
				return (HDMTREEITEM)NULL;
			DM::LPTVITEMEX pData = GetItem(hItem);

			HDMTREEITEM hRet = (HDMTREEITEM)NULL;
			if (pData && !pData->bCollapsed)
			{
				hRet = GetChildItem(hItem);
				if (hRet)
				{
					return hRet;
				}
			}
			HDMTREEITEM hParent = hItem;
			while (hParent)
			{
				hRet = GetNextSiblingItem(hParent);
				if (hRet)
				{
					return hRet;
				}
				hParent = GetParentItem(hParent);
			}
			return NULL;
		}

		HDMTREEITEM DUITransitionTreeCtrl::GetPrevVisibleItem(HDMTREEITEM hItem)
		{
			if (hItem == DMTVI_ROOT)
			{
				return (HDMTREEITEM)m_hRootFirst;
			}

			HDMTREEITEM hRet = (HDMTREEITEM)NULL;
			HDMTREEITEM hParent = hItem;
			hRet = GetPrevSiblingItem(hParent);
			if (hRet)
			{
				if (DMTVI_ROOT == hRet)
				{
					return (HDMTREEITEM)m_hRootFirst;
				}

				DM::LPTVITEMEX pData = GetItem(hRet);
				HDMTREEITEM hChild = GetChildItem(hRet, false);
				if (hChild && !pData->bCollapsed)
				{
					return hChild;
				}
				else
					return hRet;
			}
			else
				return GetParentItem(hParent);

			return NULL;
		}

		void DUITransitionTreeCtrl::OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags)
		{
			HDMTREEITEM hItem = GetSelectedItem();
			do
			{
				if (NULL == hItem)
				{
					break;
				}

				if (VK_DOWN == nChar)
				{
					HDMTREEITEM hNextItem = GetNextVisibleItem(hItem);
					if (NULL == hNextItem)
						break;
					SelectItem(hNextItem);
				}
				else if (VK_UP == nChar)
				{
					HDMTREEITEM hPrevItem = GetPrevVisibleItem(hItem);
					if (NULL == hPrevItem)
						break;
					SelectItem(hPrevItem);
				}
				else if (VK_LEFT == nChar)
				{
					Expand(hItem, DMTVEX_COLLAPSE);
				}
				else if (VK_RIGHT == nChar)
				{
					Expand(hItem, DMTVEX_EXPAND);
				}
				else if (VK_DELETE == nChar && GetSelectedItem())
				{
					TransitionMenuId TransMenuId = TransitionMenu_MAX;
					TagTransitionTree Data = (TagTransitionTree)GetItemData(GetSelectedItem());
					if (Data == TagTransitionNode)
					{
						TransMenuId = TransitionMenu_DeleTransition;
					}
					else if (Data == TagConditionNode)
					{
						TransMenuId = TransitionMenu_DeleCondition;
					}
					else if (Data == TagConditionEventNode)
					{
						TransMenuId = TransitionMenu_DeleEvent;
					}
					else if (Data == TagTargetNode)
					{
						TransMenuId = TransitionMenu_DeleTarget;
					}

					if (TransMenuId != TransitionMenu_MAX)
					{
						m_hMenuHoverItem = GetSelectedItem();
						OnCommand(0, TransMenuId, NULL);
					}
				}
			} while (false);
			SetMsgHandled(FALSE);
		}

		void DUITransitionTreeCtrl::DrawItem(IDMCanvas* pCanvas, CRect& rc, HDMTREEITEM hItem)
		{
			do
			{
				if (NULL == hItem
					|| DMTVI_ROOT == hItem)
				{
					break;
				}

				CRect rcClient;
				DV_GetClientRect(rcClient);

				//1.绘制背景
				CRect rcItemBg(rcClient.left, rc.top, rcClient.right, rc.bottom);
				DUIWND_STATE iState = DUIWNDSTATE_Normal;
				if (hItem == m_hSelItem)
				{
					iState = DUIWNDSTATE_PushDown;
				}
				else if (hItem == m_hHoverItem)
				{
					iState = DUIWNDSTATE_Hover;
				}
				if (m_pItemBgSkin)
				{
					m_pItemBgSkin->Draw(pCanvas, rc, iState);
				}
				else if (!m_crItemBg[iState].IsTextInvalid())
				{
					pCanvas->FillSolidRect(rc, m_crItemBg[iState]);
				}


				int iXOffset = GetItemXOffset(hItem);

				// 2.绘制小三角和checkbox
				DM::LPTVITEMEX pData = GetItem(hItem);
				int iOffset = 0;
				if (m_pCheckSkin&&m_bCheckBox)//先绘checkbox
				{
					iOffset = m_szCheck.cx;
					CRect rcCheck(rc.left - m_szCheck.cx, rc.top, rc.left, rc.bottom);
					MeetRect(rcCheck, m_szCheck);
					int iImgState = IIF_STATE3(pData->dwCheckBoxState, 0, 1, 2);
					if (pData->iCheckValue == DMTVEXCheckBox_Checked)
					{
						iImgState += 3;
					}
					else if (pData->iCheckValue == DMTVEXCheckBox_PartChecked)
					{
						iImgState += 6;
					}

					m_pCheckSkin->Draw(pCanvas, rcCheck, iImgState);
				}

				if (pData->bHasChildren && m_pToggleSkin)
				{
					int iImgState = IIF_STATE3(pData->dwToggleState, 0, 1, 2);
					if (!pData->bCollapsed)
					{
						iImgState += 3;
					}
					m_pToggleSkin->GetStateSize(m_szToggle);
					CRect rcToggle(rc.left - iOffset - m_szToggle.cx + iXOffset, rc.top, rc.left - iOffset + iXOffset, rc.bottom);
					MeetRect(rcToggle, m_szToggle);
					m_pToggleSkin->Draw(pCanvas, rcToggle, iImgState);
				}

				// 绘制面板
				rc.right = rc.right > rcClient.right ? rcClient.right : rc.right;
				pData->pPanel->DrawItem(pCanvas, rc);
			} while (false);
		}

		int DUITransitionTreeCtrl::GetItemWidth(HDMTREEITEM hItem)
		{
			int iWidth = 0;
			do
			{
				if (NULL == hItem
					|| DMTVI_ROOT == hItem)
				{
					break;
				}
				DM::LPTVITEMEX pData = GetItem(hItem);
				iWidth = pData->iWidth;

				//lzlong
				CRect rcClient;
				DV_GetClientRect(rcClient);
				if (rcClient.IsRectEmpty())
				{
					break;
				}
				int iXOffset = GetItemXOffset(hItem);
				iWidth = rcClient.Width() - iXOffset;
			} while (false);
			return iWidth;
		}
		void DUITransitionTreeCtrl::UpdateScrollRange()
		{
			int iHei = GetTotalHeight();
			CSize szView(0, iHei);
			SetRangeSize(szView);
		}

		void DUITransitionTreeCtrl::UpdateVisibleMap()
		{
			DMMapT<HDMTREEITEM, CRect>::RemoveAll();
			CRect rcClient;
			DV_GetClientRect(rcClient);
			if (rcClient.IsRectEmpty())
			{
				return;
			}

			HDMTREEITEM hItem = GetNextItem(DMTVI_ROOT);// 从根结点开始向下查找
			int iTotalHei = 0; int iXOffset = 0;
			while (hItem)
			{
				if (NULL == hItem || DMTVI_ROOT == hItem)
				{
					break;
				}

				DM::LPTVITEMEX pData = GetItem(hItem);
				if (pData->bVisible)
				{
					int iwidth = GetItemWidth(hItem);
					CRect rcLayout(0, 0, rcClient.Width()/*GetItemWidth(hItem)*/, pData->iHeight);
					pData->pPanel->DM_FloatLayout(rcLayout);

					if ((iTotalHei >= m_ptCurPos.y && iTotalHei < m_ptCurPos.y + rcClient.Height())
						|| (iTotalHei + pData->iHeight >= m_ptCurPos.y && iTotalHei + pData->iHeight < m_ptCurPos.y + rcClient.Height())
						|| (iTotalHei <= m_ptCurPos.y && iTotalHei + pData->iHeight >= m_ptCurPos.y + rcClient.Height())
						)
					{
						iXOffset = GetItemXOffset(hItem);
						CRect rcItem(0/*iXOffset*/, iTotalHei, iXOffset + GetItemWidth(hItem), iTotalHei + pData->iHeight);// 在大平面的坐标，以大平面左上角为原点
						rcItem.OffsetRect(rcClient.TopLeft() - m_ptCurPos);// 转换成rcClient所在的坐标系坐标
						if (m_hSelItem != hItem)
						{
							pData->pPanel->OnSetFocusWnd(NULL);
						}
						DMMapT<HDMTREEITEM, CRect>::AddKey(hItem, rcItem);
					}
					iTotalHei = iTotalHei + pData->iHeight;
					if (iTotalHei >= m_ptCurPos.y + rcClient.Height())// 总高度已超过可视区
					{
						break;
					}
				}

				if (pData->bCollapsed)
				{// 跳过被折叠的项
					HDMTREEITEM hChild = GetChildItem(hItem, false);
					while (hChild)
					{
						hItem = hChild;
						hChild = GetChildItem(hItem, false);
					}
				}
				hItem = GetNextItem(hItem);
			}
		}

		void DUITransitionTreeCtrl::LoadBranch(HDMTREEITEM hParent, DMXmlNode &XmlItem, HDMTREEITEM hInsertAfter)
		{
			DM::CArray<DUIListBox*> ComboListArray;
			while (XmlItem.IsValid())
			{
				HDMTREEITEM hItem = InsertItem(XmlItem, hParent, hInsertAfter);// 返回当前插入结点
				//下面绑定combobox select change 的事件函数
				DM::LPTVITEMEX Data = GetItem(hItem);
				if (Data && Data->pPanel)
				{
					DUIComboBox* pCombobox = Data->pPanel->FindChildByNameT<DUIComboBox>(COMBOBOXNAME);
					if (pCombobox)
					{
						DMSlotFunctorBase* pComboChgFunBase = (DMSlotFunctorBase*)XmlItem.AttributeInt64(COMBOBOXSELCHGADDR);
						DMSlotFunctorBase* pComboEditEnterFunBase = (DMSlotFunctorBase*)XmlItem.AttributeInt64(COMBOEDITINPUTENTERADDR);
						DMSlotFunctorBase* pComboEditChgFunBase = (DMSlotFunctorBase*)XmlItem.AttributeInt64(COMBOEDITCHGADDR);
						if (pComboChgFunBase)
						{
							pCombobox->GetListBox()->m_EventMgr.SubscribeEvent(DMEventLBSelChangedArgs::EventID, *pComboChgFunBase);
							delete pComboChgFunBase;
						}
						if (pComboEditEnterFunBase)
						{
							pCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DM::DMEventREWantReturnArgs::EventID, *pComboEditEnterFunBase);
							delete pComboEditEnterFunBase;
						}
						if (pComboEditChgFunBase)
						{
							pCombobox->m_pEdit->SetAttribute(L"text", pCombobox->m_pEdit->m_pDUIXmlInfo->m_strText);
							pCombobox->m_pEdit->SetEventMask(ENM_CHANGE | pCombobox->m_pEdit->GetEventMask());
							pCombobox->m_pEdit->m_EventMgr.SubscribeEvent(DMEventRENotifyArgs::EventID, *pComboEditChgFunBase);
							delete pComboEditChgFunBase;
						}
						ComboListArray.Add(pCombobox->GetListBox());
					}
				}

				DMXmlNode XmlChild = XmlItem.FirstChild(DMAttr::DUITreeCtrlExAttr::NODE_treeitem);
				if (XmlChild.IsValid())
				{// 子分支递归
					LoadBranch(hItem, XmlChild, DMTVI_LAST);
				}
				XmlItem = XmlItem.NextSibling(DMAttr::DUITreeCtrlExAttr::NODE_treeitem);
			}

			//初始化的时候触发一下
			for (size_t i = 0; i < ComboListArray.GetCount(); i++)
			{
				DMEventLBSelChangedArgs EvtSelChanged(ComboListArray[i]);
				EvtSelChanged.m_nOldSel = ManualInvokeToShowPsData;
				EvtSelChanged.m_nNewSel = 0;
				ComboListArray[i]->m_EventMgr.FireEvent(EvtSelChanged);
			}
		}

		void DUITransitionTreeCtrl::OnNodeFree(DM::LPTVITEMEX &pItemData)
		{
			if (pItemData->lParam)
			{
				TransitionTreeDataPtr pData = (TransitionTreeDataPtr)pItemData->lParam;
				if (pData && pData->m_pJSonParser)
				{
					if (0 == _wcsicmp(pData->m_pJSonParser->V_GetClassName(), EDConditionsNodeParser::GetClassName()))
					{
						EDConditionsNodeParser* pCdtNodePs = dynamic_cast<EDConditionsNodeParser*>(pData->m_pJSonParser);
						if (pCdtNodePs)
						{
							pCdtNodePs->m_bTreeItemCollapsed = pItemData->bCollapsed;
						}
					}
					else if (0 == _wcsicmp(pData->m_pJSonParser->V_GetClassName(), EDTargetsNodeParser::GetClassName()))
					{
						EDTargetsNodeParser* pTargetNodePs = dynamic_cast<EDTargetsNodeParser*>(pData->m_pJSonParser);
						if (pTargetNodePs)
						{
							pTargetNodePs->m_bTreeItemCollapsed = pItemData->bCollapsed;
						}
					}
				}
			
				pItemData->lParam = NULL;
				delete pData;
			}
			DUITreeCtrlEx::OnNodeFree(pItemData);
			m_pCurShowJsonParser = NULL;
		}

		int DUITransitionTreeCtrl::ItemHitTest(HDMTREEITEM hItem, CPoint &pt)
		{
			int iHitTestBtn = DMTVEXBtn_None;
			do
			{
				if (NULL == hItem || DMTVI_ROOT == hItem)
				{
					break;
				}
				DM::LPTVITEMEX pData = GetItem(hItem);
				if (!pData->bVisible)
				{
					break;
				}

				if (pData->bHasChildren && m_pToggleSkin)
				{
					int iXOffset = GetItemXOffset(hItem);
					CRect rcItem;
					if (!GetItemRect(hItem, rcItem))
					{
						break;
					}

					m_pToggleSkin->GetStateSize(m_szToggle);
					CRect rcToggle(rcItem.left - m_szToggle.cx + iXOffset, rcItem.top, rcItem.left + iXOffset, rcItem.bottom);
					MeetRect(rcToggle, m_szToggle);
					CPoint Pt = pt + rcItem/*m_rcWindow*/.TopLeft();
					if (rcToggle.PtInRect(Pt))
					{
						iHitTestBtn = DMTVEXBtn_Toggle;
						break;
					}
				}
			} while (false);
			return iHitTestBtn;
		}
}//end namespace ED