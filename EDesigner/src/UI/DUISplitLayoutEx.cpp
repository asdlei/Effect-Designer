#include "StdAfx.h"
#include "DUISplitLayoutEx.h"

namespace DM
{
	DUISplitLayoutEx::DUISplitLayoutEx() :m_bHideOnePartition(false)
	{
	}

	DUISplitLayoutEx::~DUISplitLayoutEx()
	{
	}

	DMCode DUISplitLayoutEx::DV_UpdateChildLayout()
	{
		DMCode iErr = DM_ECODE_FAIL;
		if (!m_bHideOnePartition)
		{
			return DUISplitLayout::DV_UpdateChildLayout();
		}
		else
		{
			if (-1 == m_iFixWid)//第一次初始化
			{
				DUISplitLayout::DV_UpdateChildLayout();
				return DV_UpdateChildLayout();
			}
			do 
			{
				if (POS_INIT == m_rcWindow.left&&POS_INIT == m_rcWindow.right&&POS_INIT == m_rcWindow.top&&POS_INIT == m_rcWindow.bottom)
				{
					DMASSERT_EXPR(0, L"窗口未完成自身布局!");
					break;
				}

				CRect rcFirst = m_rcWindow;
				m_Node.m_pFirstChild->DV_Arrange(rcFirst);
				CRect rcSecond = m_rcWindow;
				rcSecond.left = rcFirst.right;

				m_Node.m_pLastChild->DV_Arrange(rcSecond);
				iErr = DM_ECODE_OK;
			} while (false);
		}
		return iErr;
	}

	void DUISplitLayoutEx::DM_OnPaint(IDMCanvas* pCanvas)
	{
		if (!m_bHideOnePartition)
		{
			DUISplitLayout::DM_OnPaint(pCanvas);
		}
		else
		{
			DUIWindow::DM_OnPaint(pCanvas);
		}
	}
}