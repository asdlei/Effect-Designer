// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	NewResDlg.h
// File mark:   
// File summary:新建资源窗口
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-7-23
// ----------------------------------------------------------------
#pragma once

class DUIRecentListBox : public DUIListBox
{
	DMDECLARE_CLASS_NAME(RecentListBox, L"recentlist", DMREG_Window)
public: 
	DUIRecentListBox();
	DM_BEGIN_MSG_MAP()
		MSG_WM_LBUTTONDBLCLK(OnLButtonDbClick)
		MSG_WM_KEYDOWN(OnKeyDown)
	DM_END_MSG_MAP()
	void OnLButtonDbClick(UINT nFlags,CPoint pt);
	void OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags);
};

class NewResDlg : public DMHDialog
{
public:
	DECLARE_MESSAGE_MAP()
	DECLARE_EVENT_MAP()
	BOOL OnInitDialog(HWND wndFocus, LPARAM lInitParam);

	DMCode OnOpenResPath();
	DMCode OnOpenRecentDir(DMEventArgs *pEvt);
	
protected:
	DMCode OnOK();

public:
	DUIEdit*							      m_pResNameEdit;
	DUIEdit*                                  m_pResPathEdit;
	DUIListBox*                               m_pRecentList;

	CStringW								  m_strProjName;					
	CStringW								  m_strProjPath;					///< 工程所在目录(如E:\\)
	CStringW								  m_strResDir;						///< 资源文件夹全路径(m_strResPath+m_strResName，)
};