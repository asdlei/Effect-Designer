// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DUIAlignmentFrame.h
// File mark:   
// File summary:�ؼ��ƶ�������
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-4-8
// ----------------------------------------------------------------

#pragma once

enum AlignmentType { AlignmentNone = -1, AlignmentLeft, AlignmentTop, AlignmentRight, AlignmentBottom };
class DUIAlignmentFrame : public DUIStatic
{
	DMDECLARE_CLASS_NAME(DUIAlignmentFrame, DUINAME_AlignmentFrame, DMREG_Window)

public:
	DUIAlignmentFrame();
	~DUIAlignmentFrame();

	DM_BEGIN_MSG_MAP()
		DM_MSG_WM_PAINT(DM_OnPaint)
	DM_END_MSG_MAP()

	void SetRootWndRect(const CRect& rect);
	void SetAlignmentType(AlignmentType type);
	void DM_OnPaint(IDMCanvas* pCanvas);

private:
	DMColor m_Clr;
	CRect	m_RootWndRect;
	AlignmentType    m_uAlignment;
};

