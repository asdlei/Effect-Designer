#pragma once

namespace ED
{
#define TREECHECKNAME  L"treeeyecheck"
#define FIXNAILICONAME L"fixnailicon"
#define TREESTATEXT	   L"treestatext"
#define TREEITEMEDIT   L"treeitemedit"
#define TREEADDBUTTON  L"treeaddbutton"
	class DUIEffectTreeCtrl : public DUITreeCtrlEx
	{
		DMDECLARE_CLASS_NAME(DUIEffectTreeCtrl, DUINAME_EffectTreeCtrl, DMREG_Window)

		DM_BEGIN_MSG_MAP()
			MSG_WM_RBUTTONDOWN(OnRButtonDown)
			MSG_WM_LBUTTONDOWN(OnLButtonDown)
			MSG_WM_LBUTTONDBLCLK(OnLButtonDbClick)
			MSG_WM_KEYDOWN(OnKeyDown)
		DM_END_MSG_MAP()

	public:
		DUIEffectTreeCtrl();
		~DUIEffectTreeCtrl();

	protected:
		void DeleteItemNoFree(HDMTREEITEM hItem);
		HDMTREEITEM InsertItemNoData(HDMTREEITEM hInsertItem, HDMTREEITEM hParent = DMTVI_ROOT, HDMTREEITEM hInsertAfter = DMTVI_LAST);
	public:
		HDMTREEITEM MoveItemToNewPos(HDMTREEITEM hMoveItem, HDMTREEITEM hParent = DMTVI_ROOT, HDMTREEITEM hInsertAfter = DMTVI_LAST); //lzlong 更换位置

		void OnRButtonDown(UINT nFlags, CPoint pt);
		void OnLButtonDown(UINT nFlags, CPoint pt);
		void OnLButtonDbClick(UINT nFlags, CPoint pt);
		void OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags);
	public:
		DMCode OnTreeAddBtnClicked(DMEventArgs *pEvt);
		DMCode OnTreeEyeBtnClicked(DMEventArgs *pEvt);
		DMCode RenameTreeItem(HDMTREEITEM hItem);		
		DMCode RenameTreeItem(DM::LPTVITEMEX pData, CStringW strNewName);
		bool HoverItem(HDMTREEITEM hItem, bool bEnsureVisible = true);

		HDMTREEITEM InsertRootItem(CStringW strItemName, EffectTempletType data, HDMTREEITEM hInsertAfter = DMTVI_LAST, bool bEnsureVisible = false, bool bSelectItem = false, HDMTREEITEM hParent = NULL);
		HDMTREEITEM InsertChildItem(CStringW strItemName, HDMTREEITEM hParent, HDMTREEITEM hInsertAfter = DMTVI_LAST, bool bEnsureVisible = false, bool bSelectItem = false, CStringW strDefaultIcon = L"ds_effectnode");

		void OnNodeFree(DM::LPTVITEMEX &pItemData) override;
		bool IsItemStillExist(const DM::LPTVITEMEX &pItemData, LPARAM lp);
		virtual int ItemHitTest(HDMTREEITEM hItem, CPoint &pt);

		bool HasEffectTypeRootNode(int type);
		void SetTreeItemEyeBtnChk(HDMTREEITEM hItem, bool bVisible, bool bdisableBtn); //设置item的眼睛是否选中 递归  bdisableBtn表示eye不可见是否是disable的方式
		
		bool IsItemEyeChkAndNotDisable(HDMTREEITEM hItem);
		void TriggerClickItemEye(HDMTREEITEM hItem);
		bool IsItemEyeChk(HDMTREEITEM hItem);
		bool SetItemEyeVisible(HDMTREEITEM hItem, bool bVisible);
		bool SetFixnailiconVisible(HDMTREEITEM hItem, bool bVisible);
		bool IsItemEyeDisable(HDMTREEITEM hItem);
	protected:
		virtual HDMTREEITEM InsertItem(DMXmlNode &XmlItem, HDMTREEITEM hParent = DMTVI_ROOT, HDMTREEITEM hInsertAfter = DMTVI_LAST, bool bEnsureVisible = false);
		virtual void DrawItem(IDMCanvas* pCanvas, CRect& rc, HDMTREEITEM hItem);
		virtual int GetItemWidth(HDMTREEITEM hItem);
		virtual void UpdateScrollRange();
		virtual void UpdateVisibleMap();

		DMCode OnTreeItemEditKillFocus(DMEventArgs* pEvent);
		DMCode OnTreeItemEditInputReturn(DMEventArgs* pEvent);
		DMCode OnTreeItemSelectChanged(DMEventArgs* pEvent);

		HDMTREEITEM GetNextVisibleItem(HDMTREEITEM hItem);
		HDMTREEITEM GetPrevVisibleItem(HDMTREEITEM hItem);
	private:
		DMCode UpdateEye(HDMTREEITEM hItem, bool bVisible);
		CArray<DM::LPTVITEMEX>			m_DeletedItemsDataArr;
		CArray<LPARAM>					m_DeletedItemsLparamArr;
		HDMTREEITEM						m_hOldSelectItem;
	};
}
