// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	DuiTransitionTreeCtrl.h
// File mark:   
// File summary:transition下面的树控件
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-4-28
// ----------------------------------------------------------------
#pragma once

namespace ED
{
#define TREESTATICTEXT		L"treestatictext"
#define COMBOBOXNAME		L"combobox"
#define COMBOBOXSELCHGADDR	L"ComboItemChangedFunctorAddr"
#define COMBOEDITINPUTENTERADDR	L"ComboEditInputEnterFunctorAddr"
#define COMBOEDITCHGADDR		L"ComboEditChangedFunctorAddr"

#define ManualInvokeToSetPsData -100
#define ManualInvokeToShowPsData -101

#ifndef EnumToStr
#define EnumToStr(e) L###e
#endif

	//树节点节点类型
	enum TagTransitionTree
	{
		TagTransitionNULL,
		TagTransitionMajor,//Transition 顶层节点
		TagTransitionNode, //Transition 子节点
		TagConditionNode,  //Condition 子节点
		TagConditionEventNode, //Event 子节点
		TagTargetNode, //Target 子节点
	};

	// 结构体
	struct GlbCombo
	{
		int        id;
		CStringW   text;
	};

	// 结构体
	struct GlbComboEx
	{
		int        id;
		CStringW   text;
		CStringW   JStext;
	};
	//condition下面的preState和TargetState类型 
	enum EmTagState
	{
		PausedInTheFirstFrame,
		Playing,
		Paused,
		PausedInTheLastFrame,
		Invisible,
	};
	_declspec(selectany) GlbComboEx g_StateComboText[] = \
	{
		{ PausedInTheFirstFrame, L"Paused In The First Frame", L"first_frame"},//0 
		{ Playing, L"Playing", L"playing" },//1 
		{ Paused, L"Paused", L"paused" }, //2
		{ PausedInTheLastFrame, L"Paused In The Last Frame", L"last_frame" },//3
		{ Invisible, L"Invisible", L"invisible" },//4
	};
	//condition下面的event类型
	enum EmTagEventType
	{
		ActionEvent,
		StickerEvent,
		EventTypeMaxIndex,
	};
	_declspec(selectany) GlbCombo g_EventTypeComboText[EventTypeMaxIndex] = \
	{
		{ ActionEvent, L"Action Event" },//1 
		{ StickerEvent, L"Sticker Event" }, //2
	};
	//event下面的Action Event类型对应的列表
	enum EmTagActionEventList
	{
		Face,
		Blink,
		OpenMouth,
		ShakeHead,
		NodHead,
		BrowJump,
		Ok,
		Scissor,
		ThumbUp,
		Palm,
		Pistol,
		Love,
		HoldUp,
		Congratulate,
		FingerHeart,
		IndexFinger,
		HAND666,
		BLESS,
		ILOVEYOU,
		ForeGround,
		Background,
		ActionEventListMaxIndex,
	};
	_declspec(selectany) GlbComboEx g_ActionEventListComboText[ActionEventListMaxIndex] = \
	{
		{ Face, L"Face", L"face"},//0 
		{ Blink, L"Blink", L"eye_blink" },//1 
		{ OpenMouth, L"Open Mouth", L"mouth_ah" }, //2
		{ ShakeHead, L"Shake Head", L"head_yaw" },
		{ NodHead, L"NodeHead", L"head_pitch" },
		{ BrowJump, L"Brow Jump", L"brow_jump" },
		{ Ok, L"Ok", L"hand_ok" },
		{ Scissor, L"Scissor", L"hand_scissor" },
		{ ThumbUp, L"Thumb Up", L"hand_good" },
		{ Palm, L"Palm", L"hand_palm" },
		{ Pistol, L"Pistol", L"hand_pistol" },
		{ Love, L"Love", L"hand_love" },
		{ HoldUp, L"Hold Up", L"hand_holdup" },
		{ Congratulate, L"Congratulate", L"hand_congratulate" },
		{ FingerHeart, L"Finger Heart", L"hand_finger_heart" },
		{ IndexFinger, L"Index Finger", L"hand_finger_index" },
		{ HAND666, L"666", L"hand_666" },
		{ BLESS, L"BLESS", L"hand_bless" },
		{ ILOVEYOU, L"I Love You", L"hand_iloveyou"},
		{ ForeGround, L"ForeGround",  L"front"},
		{ Background, L"Background", L"background" },
	};

	//event下面的Action Event类型对应出现的类型
	enum EmTagActionEventAppearType
	{
		Appear,
		Disappear,
	};
	//event下面的Sticker Event类型对应出现的类型
	enum EmTagStickerEventAppearType
	{
		Begin,
		Play,
		Pause,
		End,
		Hide,
	};

	class DUITransitionTreeCtrl : public DUITreeCtrlEx
	{
		DMDECLARE_CLASS_NAME(DUITransitionTreeCtrl, DUINAME_TransitionTreeCtrl, DMREG_Window)
		DECLARE_EVENT_MAP()

		DM_BEGIN_MSG_MAP()
			MSG_WM_RBUTTONDOWN(OnRButtonDown)
			MSG_WM_LBUTTONDOWN(OnLButtonDown)
			MSG_WM_LBUTTONDBLCLK(OnLButtonDbClick)
			MSG_WM_KEYDOWN(OnKeyDown)
		DM_END_MSG_MAP()

	public:
		DUITransitionTreeCtrl();
		~DUITransitionTreeCtrl();

		void OnCommand(UINT uNotifyCode, int nID, HWND wndCtl, EDJSonParserPtr pJsonParser = NULL, bool bForceRefresh = false);
		DMCode OnTransitionMenuExpand();
		DMCode PopTransitionTreeMenu(HDMTREEITEM hItem);
		void OnRButtonDown(UINT nFlags, CPoint pt);
		void OnLButtonDown(UINT nFlags, CPoint pt);
		void OnLButtonDbClick(UINT nFlags, CPoint pt);
		void OnKeyDown(TCHAR nChar, UINT nRepCnt, UINT nFlags);
	public:
		DMCode OnTreeAddBtnClicked(DMEventArgs *pEvt);
		DMCode RenameTreeItem(HDMTREEITEM hItem);
		DMCode RenameTreeItemWithNoEdit(HDMTREEITEM hItem, CStringW strName);
		CStringW GetTreeItemName(HDMTREEITEM hItem);
		DUIComboBox* GetTreeItemCombobox(HDMTREEITEM hItem);
		bool HoverItem(HDMTREEITEM hItem, bool bEnsureVisible = true);

		void LoadBranch(HDMTREEITEM hParent, DMXmlNode &XmlItem, HDMTREEITEM hInsertAfter = DMTVI_LAST);
		void OnNodeFree(DM::LPTVITEMEX &pItemData) override;
		virtual int ItemHitTest(HDMTREEITEM hItem, CPoint &pt);

		DMXmlNode InitTransitionItemXml(DMXmlNode &XmlItemIn, CStringW strTransitionName = L"Transition1"); //Transition 节点
		DMXmlNode InitConditionOrTargetItemXml(DMXmlNode &XmlItemIn, const CStringW& staShowName); //conditon target 节点

		void GetParserElementListArray(CArray<CStringW>& ListArray, CStringW strItem, int& index);
		DMXmlNode InitConditionMajorXml(DMXmlNode &XmlItem, EDConditionsNodeParser* pJsonParser = NULL, CStringW strConditionName = L"Condition");	//Condition 大节点
		DMXmlNode InitEventMajorXml(DMXmlNode &XmlItem, CStringW strEventState, CStringW strEventName = L"Event");	//Event 大节点
		DMXmlNode InitTargetMajorXml(DMXmlNode &XmlItem, EDTargetsNodeParser* pJsonParser = NULL, CStringW strTargetName = L"Target");	//Target 大节点
		DMXmlNode InitRandomShowNodeXml(DMXmlNode &XmlItem, EDTransitionsNodeParser* pJsonParser = NULL, CStringW strRandowShowName = L"RandomShow");	//RandowShow 节点

		template <typename... TS>    // 模板形参包（template parameter pack）
		void InsertComboXmlItem(DMXmlNode &XmlItem, TS... args)    // 函数形参包（function parameter pack）
		{
			int nCount = (int)m_ComboListArray.GetCount();
			for (int i = 0; i < nCount; i++)
			{
				DMXmlNode ItemNode = XmlItem.InsertChildNode(L"item");
				ItemNode.SetAttribute(L"text", m_ComboListArray[i]);
			}
		}
		template <typename T>
		T CopyComboListItem(T t)
		{
			m_ComboListArray.InsertAt(0, t);
			return t;
		}

		template<class ... Types>
		inline DMXmlNode InitTreeItemNodeXml(DMXmlNode &XmlItemIn, const CStringW& staShow, DMSlotFunctorBase* pComboSeleChgFunBaseAddr, int iCurSel, Types... args) //普通的子节点	
		{
			DMXmlNode SublistXmlItem;
			DMXmlNode XmlItem = InitTreeItemNodeXmlFun(XmlItemIn, SublistXmlItem, staShow, pComboSeleChgFunBaseAddr, iCurSel);

			m_ComboListArray.RemoveAll();
			InsertComboXmlItem(SublistXmlItem, CopyComboListItem(args)...);
			Init_Debug_XmlBuf(XmlItemIn);
			return XmlItem;
		}

		inline DMXmlNode InitTreeItemNodeXml(DMXmlNode &XmlItemIn, const CStringW& staShow, DMSlotFunctorBase* pComboSeleChgFunBaseAddr, int iCurSel, CArray<CStringW> ComboListArray) //普通的子节点	
		{
			DMXmlNode SublistXmlItem;
			DMXmlNode XmlItem = InitTreeItemNodeXmlFun(XmlItemIn, SublistXmlItem, staShow, pComboSeleChgFunBaseAddr, iCurSel);

			int nCount = (int)ComboListArray.GetCount();
			for (int i = 0; i < nCount; i++)
			{
				DMXmlNode ItemNode = SublistXmlItem.InsertChildNode(L"item");
				ItemNode.SetAttribute(L"text", ComboListArray[i]);
			}
			Init_Debug_XmlBuf(XmlItemIn);
			return XmlItem;
		}

		inline DMXmlNode InitTreeItemNodeXmlFun(DMXmlNode &XmlItemIn, DMXmlNode &SublistXmlItem, const CStringW& staShow, DMSlotFunctorBase* pComboSeleChgFunBaseAddr, int iCurSel)
		{
			DMXmlNode XmlItem = XmlItemIn.InsertChildNode(L"treeitem");
			XmlItem.SetAttributeInt64(COMBOBOXSELCHGADDR, (INT64)pComboSeleChgFunBaseAddr);//function 地址传入
			XmlItem.SetAttribute(L"bcollapsed", L"0");
			if (!staShow.IsEmpty())
			{
				DMXmlNode StaXmlItem = XmlItem.InsertChildNode(L"static");
				StaXmlItem.SetAttribute(L"name", TREESTATICTEXT); StaXmlItem.SetAttribute(L"pos", L"52,0,@80,-0"); StaXmlItem.SetAttribute(L"text", staShow);
				StaXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,dd)");  StaXmlItem.SetAttribute(L"clrtextdisable", L"rgba(80,80,80,dd)"); StaXmlItem.SetAttribute(L"align", L"left");
				StaXmlItem.SetAttribute(L"alignv", L"center"); StaXmlItem.SetAttribute(L"bdot", L"1"); StaXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:150");
			}

			DMXmlNode ComboXmlItem = XmlItem.InsertChildNode(L"combobox");
			ComboXmlItem.SetAttribute(L"name", COMBOBOXNAME);
			ComboXmlItem.SetAttribute(L"pos", L"133,2,-5,@21"); ComboXmlItem.SetAttribute(L"clrbg", L"rgba(40,4b,57,FF)"); ComboXmlItem.SetAttribute(L"bDropTranslucent", L"1");
			ComboXmlItem.SetAttribute(L"ncmargin", L"8,2,5,2"); ComboXmlItem.SetAttribute(L"ncskin", L"ds_combframe2"); ComboXmlItem.SetAttribute(L"curSel", std::to_wstring(iCurSel).c_str()); ComboXmlItem.SetAttribute(L"btnSkin", L"ds_combarrow");
			ComboXmlItem.SetAttribute(L"bHideEdit", L"1"); ComboXmlItem.SetAttribute(L"dropHeight", L"800"); ComboXmlItem.SetAttribute(L"dotted", L"1");
			ComboXmlItem.SetAttribute(L"animateTime", L"200"); ComboXmlItem.SetAttribute(L"clrtext", L"pbgra(ff,ff,ff,d0)");  ComboXmlItem.SetAttribute(L"clrtextdisable", L"pbgra(30,30,30,d0)"); ComboXmlItem.SetAttribute(L"font", L"face:微软雅黑, size : 13, weight : 400");
			DMXmlNode SubEditXmlItem = ComboXmlItem.InsertChildNode(L"subedit");
			SubEditXmlItem.SetAttribute(L"align", L"left"); SubEditXmlItem.SetAttribute(L"bmultiLines", L"0"); SubEditXmlItem.SetAttribute(L"sbskin", L"ds_scrollbar"); SubEditXmlItem.SetAttribute(L"font", L"face:宋体,size:16,bold:0,underline 0,italic:0,strike:0");
			SublistXmlItem = ComboXmlItem.InsertChildNode(L"sublistbox");
			SublistXmlItem.SetAttribute(L"Skin", L"ds_combdropframe"); SublistXmlItem.SetAttribute(L"ncmargin", L"0,6,0,5"); SublistXmlItem.SetAttribute(L"textpoint", L"8,-1"); SublistXmlItem.SetAttribute(L"ncSkin", L"ds_combdropframe");
			SublistXmlItem.SetAttribute(L"bdot", L"1"); SublistXmlItem.SetAttribute(L"itemheight", L"26"); SublistXmlItem.SetAttribute(L"bhotTrack", L"1"); SublistXmlItem.SetAttribute(L"clritemtext", L"rgba(ff,ff,ff,d0)");
			SublistXmlItem.SetAttribute(L"clritemseltext", L"rgba(FF,FF,FF,d0)"); SublistXmlItem.SetAttribute(L"clritembg", L"rgba(43,4f,5a,FF)"); SublistXmlItem.SetAttribute(L"clritemselbg", L"rgba(29,3d,50,ff)"); SublistXmlItem.SetAttribute(L"clritemtext", L"rgba(ff,ff,ff,d0)");
			SublistXmlItem.SetAttribute(L"sbskin", L"ds_scrollbar"); SublistXmlItem.SetAttribute(L"arrowwidth", L"0"); SublistXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:13,weight:400");
			return XmlItem;
		}

		template<class ... Types>
		inline DMXmlNode InitTreeItemWithComboEditNodeXml(DMXmlNode &XmlItemIn, const CStringW& staShow, DMSlotFunctorBase* pComboSeleChgFunBaseAddr, DMSlotFunctorBase* pComboEditInputEnterFunBaseAddr, DMSlotFunctorBase* pComboEditChgFunBaseAddr, CStringW strEditText, Types... args) //普通的带Coboboxedit子节点	
		{
			DMXmlNode XmlItem = XmlItemIn.InsertChildNode(L"treeitem");
			XmlItem.SetAttributeInt64(COMBOBOXSELCHGADDR, (INT64)pComboSeleChgFunBaseAddr);//function 地址传入
			XmlItem.SetAttributeInt64(COMBOEDITINPUTENTERADDR, (INT64)pComboEditInputEnterFunBaseAddr);
			XmlItem.SetAttributeInt64(COMBOEDITCHGADDR, (INT64)pComboEditChgFunBaseAddr);
			if (!staShow.IsEmpty())
			{
				DMXmlNode StaXmlItem = XmlItem.InsertChildNode(L"static");
				StaXmlItem.SetAttribute(L"name", TREESTATICTEXT); StaXmlItem.SetAttribute(L"pos", L"52,0,@80,-0"); StaXmlItem.SetAttribute(L"text", staShow);
				StaXmlItem.SetAttribute(L"clrtext", L"rgba(ff,ff,ff,dd)");  StaXmlItem.SetAttribute(L"clrtextdisable", L"rgba(80,80,80,dd)"); StaXmlItem.SetAttribute(L"align", L"left");
				StaXmlItem.SetAttribute(L"alignv", L"center"); StaXmlItem.SetAttribute(L"bdot", L"1"); StaXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:12,weight:150");
			}

			DMXmlNode ComboXmlItem = XmlItem.InsertChildNode(L"combobox");
			ComboXmlItem.SetAttribute(L"name", COMBOBOXNAME);
			ComboXmlItem.SetAttribute(L"pos", L"133,2,-5,@21"); ComboXmlItem.SetAttribute(L"clrbg", L"rgba(40,4b,57,FF)"); ComboXmlItem.SetAttribute(L"bDropTranslucent", L"1");
			ComboXmlItem.SetAttribute(L"ncmargin", L"5,0,5,0"); ComboXmlItem.SetAttribute(L"ncskin", L"ds_combframe2"); ComboXmlItem.SetAttribute(L"curSel", L"0"); ComboXmlItem.SetAttribute(L"btnSkin", L"ds_combarrow");
			ComboXmlItem.SetAttribute(L"bHideEdit", L"0"); ComboXmlItem.SetAttribute(L"dropHeight", L"800"); ComboXmlItem.SetAttribute(L"dotted", L"1");
			ComboXmlItem.SetAttribute(L"animateTime", L"200"); ComboXmlItem.SetAttribute(L"clrtext", L"pbgra(ff,ff,ff,d0)");  ComboXmlItem.SetAttribute(L"clrtextdisable", L"pbgra(30,30,30,d0)"); ComboXmlItem.SetAttribute(L"font", L"face:微软雅黑, size : 13, weight : 400");
			DMXmlNode SubEditXmlItem = ComboXmlItem.InsertChildNode(L"subedit");
			SubEditXmlItem.SetAttribute(L"text", strEditText);
			SubEditXmlItem.SetAttribute(L"align", L"left"); SubEditXmlItem.SetAttribute(L"bmultiLines", L"0"); SubEditXmlItem.SetAttribute(L"bwantreturn", L"1"); SubEditXmlItem.SetAttribute(L"bnumber", L"1"); SubEditXmlItem.SetAttribute(L"sbskin", L"ds_scrollbar"); SubEditXmlItem.SetAttribute(L"clrcaret", L"pbgra(ff, ff, df, ff)");
			SubEditXmlItem.SetAttribute(L"clrtext", L"pbgra(ff, ff, ff, d0)"); SubEditXmlItem.SetAttribute(L"clrtextdisable", L"pbgra(30,30,30,d0)"); SubEditXmlItem.SetAttribute(L"skin", L"ds_editFrameCombo"); SubEditXmlItem.SetAttribute(L"rcinsertmargin", L"3, 1, 2, 0"); SubEditXmlItem.SetAttribute(L"font", L"face:微软雅黑, size : 13, weight : 400");
			DMXmlNode SublistXmlItem = ComboXmlItem.InsertChildNode(L"sublistbox");
			SublistXmlItem.SetAttribute(L"Skin", L"ds_combdropframe"); SublistXmlItem.SetAttribute(L"ncmargin", L"0,6,0,5"); SublistXmlItem.SetAttribute(L"textpoint", L"8,-1"); SublistXmlItem.SetAttribute(L"ncSkin", L"ds_combdropframe");
			SublistXmlItem.SetAttribute(L"bdot", L"1"); SublistXmlItem.SetAttribute(L"itemheight", L"26"); SublistXmlItem.SetAttribute(L"bhotTrack", L"1"); SublistXmlItem.SetAttribute(L"clritemtext", L"rgba(ff,ff,ff,d0)");
			SublistXmlItem.SetAttribute(L"clritemseltext", L"rgba(FF,FF,FF,d0)"); SublistXmlItem.SetAttribute(L"clritembg", L"rgba(43,4f,5a,FF)"); SublistXmlItem.SetAttribute(L"clritemselbg", L"rgba(29,3d,50,ff)"); SublistXmlItem.SetAttribute(L"clritemtext", L"rgba(ff,ff,ff,d0)");
			SublistXmlItem.SetAttribute(L"sbskin", L"ds_scrollbar"); SublistXmlItem.SetAttribute(L"arrowwidth", L"0"); SublistXmlItem.SetAttribute(L"font", L"face:微软雅黑,size:13,weight:400");

			m_ComboListArray.RemoveAll();
			InsertComboXmlItem(SublistXmlItem, CopyComboListItem(args)...);
			Init_Debug_XmlBuf(XmlItemIn);
			return XmlItem;
		}

		virtual HDMTREEITEM InsertItem(DMXmlNode &XmlItem, HDMTREEITEM hParent = DMTVI_ROOT, HDMTREEITEM hInsertAfter = DMTVI_LAST, bool bEnsureVisible = false);
	protected:
		virtual void DrawItem(IDMCanvas* pCanvas, CRect& rc, HDMTREEITEM hItem);
		virtual int GetItemWidth(HDMTREEITEM hItem);
		virtual void UpdateScrollRange();
		virtual void UpdateVisibleMap();

		HDMTREEITEM ComboSeleChgTreeItem(DMEventArgs* pEvent);
		DM::HDMTREEITEM EditSelChgTreeItem(DMEventArgs* pEvent);
		DMCode EnableComboItem(HDMTREEITEM hItem, BOOL bEnable, BOOL bResetSelect = FALSE);
		//combobox 选项改变事件处理
		DMCode OnPreState0ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnPreState1ComboSelectChanged(DMEventArgs* pEvent);

		DMCode OnEvent0ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnEvent1ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnEvent2ComboSelectChanged(DMEventArgs* pEvent);

		DMCode OnTarget_TargetSticker_ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnTarget_TargetState_ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnTarget_EditInputEnter(DMEventArgs* pEvent);
		DMCode OnTarget_DelayFrame_EditChanged(DMEventArgs* pEvent);
		DMCode OnTarget_DelayFrame_ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnTarget_FadeFrame_EditChanged(DMEventArgs* pEvent);
		DMCode OnTarget_FadeFrame_ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnTarget_LastingFrame_EditChanged(DMEventArgs* pEvent);
		DMCode OnTarget_LastingFrame_ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnTarget_PlayLoop_EditChanged(DMEventArgs* pEvent);
		DMCode OnTarget_PlayLoop_ComboSelectChanged(DMEventArgs* pEvent);
		DMCode OnRandowShow_ComboSelectChanged(DMEventArgs* pEvent);

		DMCode OnTreeItemEditKillFocus(DMEventArgs* pEvent);
		DMCode OnTreeItemEditInputReturn(DMEventArgs* pEvent);
		DMCode OnTreeItemSelectChanged(DMEventArgs* pEvent);

		HDMTREEITEM GetNextVisibleItem(HDMTREEITEM hItem);
		HDMTREEITEM GetPrevVisibleItem(HDMTREEITEM hItem);
	private:
		HDMTREEITEM						m_hOldSelectItem;
		HDMTREEITEM						m_hMenuHoverItem;           ///< 弹出菜单当前停留项

		CArray<CStringW>				m_ComboListArray;

		int								m_iDelayFrame;
		int								m_iFadeFrame;
		int								m_iLastingFrame;
		int								m_iPlayLoop;
	public:
		static DMCode s_DMHandleEvent(DMEventArgs *pEvt);
		static BOOL   s_ProcessWindowMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, LRESULT& lResult, DWORD dwMsgMapID = 0);

		// 模拟DMCWnd
		BOOL							m_bMsgHandled;
		// 用于转发消息
		static DUITransitionTreeCtrl*   ms_pthis;
		EDJSonParserPtr					m_pCurShowJsonParser;
	};
}
