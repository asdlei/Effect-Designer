//----------------------------------------------------------------
// Copyright (c)     
// All rights reserved.  
//          
// File name:	DUIObjEditor.h 
// File mark:   
// File summary:对象视图编辑主窗口
// Author:		lzlong
// Edition:     1.0 
// Create date: 2019-2-28
// ----------------------------------------------------------------
#pragma once
#include "DUIRoot.h" 

class DUIReferenceLine;
class DUIObjEditor : public DUIScrollBase
{
	DMDECLARE_CLASS_NAME(DUIObjEditor, L"dsobjeditor",DMREG_Window)
public:
	DUIObjEditor();

public:
	DMCode InitObjEditor();
	DMCode UnInitObjEditor();
	DMCode SetDesignMode(DesignMode ds_mode);

	DUIRoot* InitDesignChild(HDMTREEITEM hRootTree);			///< 初始化Design子窗口
	DMCode UnlinkTreeChildNode(HDMTREEITEM hItem);
	DUIRoot* GetShowDesignChild();
	DMCode ShowDesignChild(DUIRoot* pShow);
	DMCode RemoveAllDesignChild();

	// 控制滚动范围
	void UpdateScrollRangeSize();
	void OnRangeCurPosChanged(CPoint ptOld,CPoint ptNew);
	
	DMCode DV_UpdateChildLayout();

	// 控制各frame
	DMCode HoverInSelMode(DUIWindow* pDUIHover);
	DMCode AlignMentFrameInSelMode(DUIWindow* pDUIHover, AlignmentType alignType);
	DMCode HideHoverFrameWnd();
	DMCode HideAlignMentFrameWnd();
	DMCode HideDragFrameWnd();
	DMCode HideReferenceLineFrameWnd();
	DMCode DragFrameInSelMode();
	DMCode ReferenceLineFrameInSelMode();

public:
	DUIRoot*											m_pShow;
	DUIStatic*											m_pHoverlFrame;					///< 在SelectMode时框选停留DUI
	DUIAlignmentFrame*									m_pAlignmentFrame;				///< 在SelectMode时移动选中控件而出现的参考线
	DUIDragFrame*                                       m_pDragFrame;                   ///< 在SelectMode下框选 选中的树形控件  对应的DUI
	DUIReferenceLine*									m_pReferenceLine1;				///< 参考线1
	DUIReferenceLine*									m_pReferenceLine2;				///< 参考线2

	DMSmartPtrT<DUIPos>									m_pDuiPos;
	ED::DUIEffectTreeCtrl*								m_pObjTree;
	DesignMode											m_DesignMod;
	CRect												m_rcMeasure;
	bool                                                m_bInit;
};