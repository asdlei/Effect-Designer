//-------------------------------------------------------
// Copyright (c) 
// All rights reserved.
// 
// File Name: EDFaceMorphParser.h 
// File Des: FaceMorph字段  Json解析类
// File Summary: 
// Cur Version: 1.0
// Author:
// Create Data:
// History:
// 		<Author>	<Time>		<Version>	  <Des>
//      lzlong		2019-5-25	1.0		
//-------------------------------------------------------
#pragma once

namespace EDAttr
{
	class EDFaceMorphParserAttr
	{
	public:
		static char* BOOL_allowDiscardFrame;					///< 示例:"allowDiscardFrame": false
		static char* SIZE_canvasSize;							///< 示例:"canvasSize": [750, 1334],
		static char* INT_targetFPS;								///< 示例:"targetFPS": 25
	};
	EDAttrValueInit(EDFaceMorphParserAttr, BOOL_allowDiscardFrame)EDAttrValueInit(EDFaceMorphParserAttr, SIZE_canvasSize)EDAttrValueInit(EDFaceMorphParserAttr, INT_targetFPS)

	class EDMakeupsNodeParserAttr
	{
	public:
		static char* BOOL_enable;								///< 示例:"enable": true
		static char* STRING_imagePath;                          ///< 示例:"imagePath" : "__facemorph/eyeC.png"
		static char* DOUBLE_imageScale;							///< 示例:"imageScale" : 1.05
		static char* STRING_name;								///< 示例:"name" : "eyeC"
		static char* POINT_position;							///< 示例:"position" : [268, 335]
		static char* STRING_tag;								///< 示例:"tag" : "EYE"
		static char* INT_zPosition;								///< 示例:"zPosition" : 15
	};
	EDAttrValueInit(EDMakeupsNodeParserAttr, BOOL_enable)EDAttrValueInit(EDMakeupsNodeParserAttr, STRING_imagePath)EDAttrValueInit(EDMakeupsNodeParserAttr, DOUBLE_imageScale)
	EDAttrValueInit(EDMakeupsNodeParserAttr, STRING_name)EDAttrValueInit(EDMakeupsNodeParserAttr, POINT_position)EDAttrValueInit(EDMakeupsNodeParserAttr, STRING_tag)EDAttrValueInit(EDMakeupsNodeParserAttr, INT_zPosition)
}

// namespace DM
// {
	/// <summary>
	///		"FaceMorph"节点json字段解析类
	/// </summary>
	class EDFaceMorphParser : public EDJSonParser
	{
		EDDECLARE_CLASS_NAME(EDFaceMorphParser, L"faceMorph", DMREG_Attribute);
	public:
		EDFaceMorphParser();
		~EDFaceMorphParser();
		DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;

		DMCode BuildJSonData(JSHandle JSonHandler) override;
		DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;
		int	   GetZPositionOrder() override;//其实并没有ZPosition节点
	public:
		bool			m_bAllowDiscardFrame;
		CSize			m_canvasSize;
		int				m_iTargetFPS;
	};

	/// <summary>
	///		"makeups"节点json字段解析类
	/// </summary>
	class EDMakeupsParser : public EDJSonParser
	{
		EDDECLARE_CLASS_NAME(EDMakeupsParser, L"makeups", DMREG_Attribute);
	public:
		EDMakeupsParser();
		~EDMakeupsParser();

		DMCode InitJSonData(JSHandle &JSonHandler) override;
		DMCode BuildJSonData(JSHandle JSonHandler) override;
	};

	/// <summary>
	///		"makeups"每个子节点json字段解析类
	/// </summary>
	class EDMakeupsNodeParser : public EDJSonParser
	{
		EDDECLARE_CLASS_NAME(EDMakeupsNodeParser, L"makeupsNode", DMREG_Attribute);
	public:
		EDMakeupsNodeParser();
		~EDMakeupsNodeParser();
				
		DMCode SetJSonAttribute(LPCSTR pszAttribute, JSHandle& JsHandleValue, bool bLoadJSon) override;
		DMCode ParseJSNodeObjFinished() override;
		DMCode BuildMemberJsonData(JSHandle &JSonHandler) override;

		DMCode RelativeResourceNodeParser(EDResourceNodeParser* pResourceNodeParser, bool bLoadJSon = false) override; ///<关联Resource指针
		EDResourceNodeParser* GetRelativeResourceNodeParser() override;
		void SetParserItemRect(const CRect& rect) override;
		CPoint GetParserItemTopLeftPt() override;
		double GetParserImageScale() override;
		int GetZPositionOrder() override;
		bool SetZPositionOrder(int iZPostion) override;
		bool IsParserEnable() override;
	public:
		bool			m_bEnable;
		CStringW		m_strImagePath;
		double			m_dbImageScale;
		CStringW		m_strName;
		CPoint			m_ptPosition;
		CStringW		m_strTag;				///< L"NOSE" L"NONE"
		int				m_iZPosition;
	public:
		EDResourceNodeParser* m_pRelativeResourceNodeParser;
	};

//}; //end namespace DM