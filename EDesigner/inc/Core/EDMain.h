// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	EDMain.h
// File mark:   
// File summary:核心类，管理所有的资源，运行于主UI线程
// Author:		lzlong
// Edition:     1.0
// Create date: 2019-1-10
// ----------------------------------------------------------------
#pragma once


// 简易宏定义
#define  g_pMain                                EDMain::getSingletonPtr()

#define  g_pMainWnd								g_pMain->m_pMainWnd

//插入observe动作  需要观察的对象都可以加入其中，加入观察的动作可以做到redo和undo了  动作双向链表:NullActionSlot->ActionSlot->NullActionSlot->ActionSlot->NullActionSlot->
#define  INSERTNEWOBSERVEACTION(ACT)			if (g_pMainWnd->m_actionSlotMgr.IsActionArrayChainEmpty() && !g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot()){\
														g_pMainWnd->m_actionSlotMgr.InsertNewAction(new NullActionSlot());\
												}\
												g_pMainWnd->m_actionSlotMgr.InsertNewAction(ACT);\
												if (!g_pMainWnd->m_actionSlotMgr.GetMemberbMuteAddSlot())\
													g_pMainWnd->m_actionSlotMgr.InsertNewAction(new NullActionSlot());

#define  g_pCtrlXml								g_pMain->m_pCtrlXml

class EDMain : public DMSingletonT<EDMain>
{
public:
	EDMain(HINSTANCE hInst = GetModuleHandle(NULL));
	~EDMain();
public:
	bool InitMain();
	bool UnInitMain();

public:
	bool RunPrepare();														///< 加载初始信息，如数据、dump上报等
	bool RunMainUI();														///< 主UI线程启动

public:
	
public: // 唯一性对象
 	DMSmartPtrT<EDMainWnd>		m_pMainWnd;                                 ///< 主UI窗口
	DMLazyT<DMCtrlXml>			m_pCtrlXml;									///< 配置文件
};