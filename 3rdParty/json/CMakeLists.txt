PROJECT(json)	# 静态库

# 文件分组（过滤），同时必须加入到ADD_LIBRARY中才能显示
FILE(GLOB JSON_SOURCE									*.h;*.cpp)
FILE(GLOB JSON_IMPL										json/*.h;json/*.inl;json/*.cpp)

source_group(""											FILES	${JSON_SOURCE})
source_group("impl"										FILES	${JSON_IMPL})

#-------------------------------------------------------------------------------------------------------------------
# header file search
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/
					${CMAKE_CURRENT_SOURCE_DIR}/json)

#-------------------------------------------------------------------------------------------------------------------
# 输出设置
SET(Json_CODE 
	${JSON_SOURCE}
	${JSON_IMPL}
	)
	
ADD_LIBRARY(json STATIC ${Json_CODE})

#-------------------------------------------------------------------------------------------------------------------
ADD_DEFINITIONS(-DUNICODE -D_UNICODE)
ADD_DEFINITIONS(-D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS) 						# disable CRT
if(USE_ALLMT_)
SET(CMAKE_CXX_FLAGS_DEBUG "/D_DEBUG /MTd /Zi /Ob0 /Od /RTC1")   							# MTD
SET(CMAKE_CXX_FLAGS_RELEASE "/MT /Zi /O2 /Ob1 /DNDEBUG")
SET(CMAKE_CXX_FLAGS_MINSIZEREL "/MT /O1 /Ob1 /DNDEBUG")
SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "/MT /Zi /O2 /Ob1 /DNDEBUG")
endif()
SET(CMAKE_MODULE_LINKER_FLAGS_RELEASE "/FORCE:MULTIPLE")
SET(CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL "/FORCE:MULTIPLE")
SET(CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO "/FORCE:MULTIPLE")
SET_TARGET_PROPERTIES(json PROPERTIES FOLDER 3rdParty)

