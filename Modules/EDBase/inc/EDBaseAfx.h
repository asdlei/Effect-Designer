// ----------------------------------------------------------------
// Copyright (c)  
// All rights reserved.
// 
// File name:	GPBaseAfx.h
// File mark:   
// File summary:
// Author:		guoyouhuang
// Edition:     1.0
// Create date: 2017-3-9
// ----------------------------------------------------------------
#pragma once
#define WIN32_LEAN_AND_MEAN             // 从 Windows 头中排除极少使用的资料

#include "EDBaseOutput.h"