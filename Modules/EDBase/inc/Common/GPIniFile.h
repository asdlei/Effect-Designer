#pragma once

typedef  std::vector<std::wstring>    inistringlist;

class GPIniFileException
{
public:
	GPIniFileException(void){};
	~GPIniFileException(void){};
};

class ED_EXPORT GPIniFile  
{
public:
	GPIniFile(std::wstring FileName);
	GPIniFile(LPCWSTR FileName);
	virtual ~GPIniFile();
	bool SectionExists(std::wstring Section);

	std::wstring ReadString(std::wstring Section, std::wstring Ident, std::wstring Default);
	bool WriteString(std::wstring Section, std::wstring Ident, std::wstring Value);

	int ReadInt32(std::wstring Section, std::wstring Ident, int Default);
	bool WriteInt32(std::wstring Section, std::wstring Ident, int Value);
	
	LONGLONG ReadInt64(std::wstring Section, std::wstring Ident, LONGLONG Default);
	bool WriteInt64(std::wstring Section, std::wstring Ident, LONGLONG Value);
	
	int ReadIntegerHex(std::wstring Section, std::wstring Ident, int Default);
	bool WriteIntegerHex(std::wstring Section, std::wstring Ident, int Value);
	bool WriteIntegerHex(std::wstring Section, std::wstring Ident, int len, int Value);

	bool WriteFloat(std::wstring Section, std::wstring Ident, double Value);
	double ReadFloat(std::wstring Section, std::wstring Ident, double Default);
	
	bool WriteBool(std::wstring Section, std::wstring Ident, bool Value);
	bool ReadBool(std::wstring Section, std::wstring Ident, bool Default);



	void ReadSection(std::wstring Section, inistringlist &Strings);
	void ReadSections(inistringlist &Strings);
	void ReadSectionValues(std::wstring Section, inistringlist &Strings);
	bool EraseSection(std::wstring Section);

protected:
	std::wstring m_strFileName;
	std::wstring UpperCase(std::wstring value);
	std::wstring LowerCase(std::wstring value);
};